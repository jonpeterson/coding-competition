app.directive('appHeader', function() {
    return {
        restrict: 'E',
        templateUrl: 'views/header.html',
        replace: true,

        controller: ['$scope', '$modal', '$rootScope', '$http', '$route', '$cookies', 'UserModel', function(scope, modal, rootScope, http, route, cookies, UserModel) {
            scope.breadcrumbs = rootScope.breadcrumbs;

            var login = function(username, password) {
                if(password) {
                    rootScope.authUsername = username;
                    rootScope.authPassword = password;
                }

                var user = new UserModel();
                return user.load(username).then(function() {
                    rootScope.currentUser = user;
                    cookies.currentUsername = user.username;

                    route.reload();
                });
            };

            if(cookies.currentUsername)
                login(cookies.currentUsername);

            scope.showLoginModal = function() {
                var modalInstance = modal.open({
                    scope: scope,
                    templateUrl: 'views/header-modal-login.html',
                    controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                        scope.username = undefined;
                        scope.password = undefined;
                        scope.errorStatusCode = undefined;

                        scope.login = function() {
                            login(scope.username, scope.password).then(
                                function(response) {
                                    modalInstance.close();
                                },
                                function(response) {
                                    scope.errorStatusCode = response.status;
                                }
                            );
                        };

                        scope.cancel = function() {
                            modalInstance.dismiss();
                        };

                        scope.clearError = function() {
                            scope.errorStatusCode = undefined;
                        };
                    }]
                });
            };

            scope.showRegistrationModal = function() {
                var modalInstance = modal.open({
                    scope: scope,
                    templateUrl: 'views/header-modal-register.html',
                    controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                        scope.username = undefined;
                        scope.password = undefined;
                        scope.name = undefined;
                        scope.email = undefined;
                        scope.errorStatusCode = undefined;

                        scope.register = function() {
                            http({
                                method: 'PUT',
                                url: '/api/user/username/' + scope.username,
                                data: {
                                    password: scope.password,
                                    name: scope.name,
                                    email: scope.email
                                }
                            })
                            .then(function(response) {
                                scope.login = function() {
                                    login(scope.username, scope.password).then(
                                        function(response) {
                                            modalInstance.close();
                                        }
                                    );
                                };
                            }, function(response) {
                                scope.errorStatusCode = response.status;
                            });
                        };

                        scope.cancel = function() {
                            modalInstance.dismiss();
                        };

                        scope.clearError = function() {
                            scope.errorStatusCode = undefined;
                        };
                    }]
                });
            };

            scope.logout = function() {
                return http({
                    method: 'DELETE',
                    url: '/api/session/'
                })
                .then(function(response) {
                    cookies.authToken = '';
                    cookies.currentUsername = '';

                    rootScope.currentUser = undefined;
                    rootScope.authUsername = undefined;
                    rootScope.authPassword = undefined;

                    route.reload();
                });
            };
        }]
    };
});