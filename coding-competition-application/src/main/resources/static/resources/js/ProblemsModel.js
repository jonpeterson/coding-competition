app.factory('ProblemsModel', ['$http', function(http) {

    return function() {
        var self = this;

        self.load = function() {
            return http({
                method: 'GET',
                url: '/api/problem/template/'
            })
            .then(function(response) {
                self.data = {};

                angular.forEach(response.data, function(problemTemplate) {
                    self.data[problemTemplate.id] = problemTemplate;
                });
            });
        };

        self.withProblemConfigurations = function(problemConfigurations) {
            angular.forEach(problemConfigurations, function(problemConfiguration) {
                var problem = self.data[problemConfiguration.problemTemplateId];
                if(!problem)
                    return;

                problem.enabled = problemConfiguration.enabled;
                problem.points = problemConfiguration.points;
            });
        };

        self.forUserInCompetition = function(username, competitionId) {
            return http({
                method: 'GET',
                url: '/api/competition/id/' + competitionId + '/problem/byUsername/' + username
            })
            .then(function(response) {
                angular.forEach(response.data, function(userProblem) {
                    var problem = self.data[userProblem.problemTemplateId];
                    if(!problem)
                        return;

                    problem.solved = userProblem.solved;
                    problem.failedAttempts = userProblem.failedAttempts;
                });
            });
        };

        self.forTemplateInCompetition = function(problemTemplateId, competitionId) {
            return http({
                method: 'GET',
                url: '/api/competition/id/' + competitionId + '/problem/byTemplateId/' + problemTemplateId
            })
            .then(function(response) {
                angular.forEach(response.data, function(userProblem) {
                    var problem = self.data[userProblem.problemTemplateId];
                    if(!problem)
                        return;

                    problem.solved = userProblem.solved;
                    problem.failedAttempts = userProblem.failedAttempts;
                });
            });
        };
    };
}]);