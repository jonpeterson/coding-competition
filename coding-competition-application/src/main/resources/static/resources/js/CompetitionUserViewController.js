'use strict';

app.controller('CompetitionUserViewController', ['$scope', '$rootScope', '$routeParams', '$q', 'UserModel', 'CompetitionModel', 'ProblemsModel', function(scope, rootScope, routeParams, q, UserModel, CompetitionModel, ProblemsModel) {
    var user = new UserModel();
    var competition = new CompetitionModel();
    var problems = new ProblemsModel();

    q.all([
        user.load(routeParams.username),
        competition.load(routeParams.id),
        problems.load()
    ]).then(function() {
        rootScope.breadcrumbs = [
            { label: 'Competitions', link: 'competitions' },
            { label: competition.name, link: 'competition/' + competition.id },
            { label: user.name, link: '#' }
        ];

        problems.withProblemConfigurations(competition.problemConfigurations);
        problems.forUserInCompetition(user.username, competition.id).then(function() {
            scope.user = user;
            scope.competition = competition;
            scope.problems = problems;
        });
    });
}]);