'use strict';

app.controller('CompetitionProblemViewController', ['$scope', '$rootScope', '$routeParams', '$http', '$q', 'CompetitionModel', function(scope, rootScope, routeParams, http, q, CompetitionModel) {
    var competitionId = routeParams.id;
    var problemId = routeParams.problemId;

    var competition = new CompetitionModel();

    q.all([
        competition.load(routeParams.id),
        http({
            method: 'GET',
            url: '/api/competition/id/' + routeParams.id + '/problem/byTemplateId/' + problemId
        }),
        http({
            method: 'GET',
            url: '/api/problem/template/'
        })
    ]).then(function(responses) {
        rootScope.breadcrumbs = [
            { label: 'Competitions', link: 'competitions' },
            { label: competition.name, link: 'competition/' + competition.id },
            { label: problemId, link: '#' }
        ];

        var userProblems = {};
        angular.forEach(responses[1].data, function(userProblem) {
            userProblems[userProblem.username] = userProblem;
        });

        var problem;
        angular.forEach(responses[2].data, function(problemTemplate) {
            if(problemTemplate.id == problemId)
                problem = problemTemplate;
        });

        angular.forEach(competition.problemConfigurations, function(problemConfiguration) {
            if(problemConfiguration.problemTemplateId == problemId) {
                problem.points = problemConfiguration.points;
                problem.enabled = problemConfiguration.enabled;
            }
        });

        scope.problem = problem;
        scope.competition = competition;
        scope.userProblems = userProblems;
    });
}]);