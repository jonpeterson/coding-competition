app.factory('UserModel', ['$http', 'CompetitionModel', function(http, CompetitionModel) {

    return function() {
        var self = this;

        self.load = function(username, loadCompetitions) {
            if(loadCompetitions)
                http({
                    method: 'GET',
                    url: '/api/competition/byUsername/' + username
                })
                .then(function(response) {
                    self.competitions = [];
                    angular.forEach(response.data, function(competitionData) {
                        var competition = new CompetitionModel();
                        competition.set(competitionData);
                        self.competitions.push(competition);
                    });
                });

            return http({
                method: 'GET',
                url: '/api/user/username/' + username
            })
            .then(function(response) {
                self.set(response.data);
            });
        };

        self.set = function(data) {
            self.username = data.username;
            self.email = data.email;
            self.name = data.name;
            self.apiKey = data.apiKey;
            self.roles = data.roles;
        };

        self.changeName = function(newName) {
            return http({
                method: 'PUT',
                url: '/api/user/username/' + self.username + '/name',
                data: newName,
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(function(response) {
                self.load(self.username);
            });
        };

        self.changePassword = function(currentPassword, newPassword) {
            return http({
                method: 'PUT',
                url: '/api/user/username/' + self.username + '/password',
                data: {
                    currentPassword: currentPassword,
                    newPassword: newPassword,
                }
            });
        };

        self.changeEmail = function(newEmail) {
            return http({
                method: 'PUT',
                url: '/api/user/username/' + self.username + '/email',
                data: newEmail,
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(function(response) {
                self.load(self.username);
            });
        };

        self.resetApiKey = function() {
            return http({
                method: 'DELETE',
                url: '/api/user/username/' + self.username + '/apiKey'
            })
            .then(function(response) {
                self.load(self.username);
            });
        };

        self.toggleRole = function(role) {
            return http({
                method: self.hasRole(role) ? 'DELETE' : 'PUT',
                url: '/api/user/username/' + self.username + '/role/' + role
            })
            .then(function(response) {
                self.load(self.username);
            });
        };

        self.hasRole = function(role) {
            return self.roles && self.roles.indexOf(role) > -1;
        };

        self.isAdmin = function() {
            return self.hasRole('ADMIN');
        };
    };
}]);