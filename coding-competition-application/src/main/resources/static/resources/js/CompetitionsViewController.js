'use strict';

app.controller('CompetitionsViewController', ['$scope', '$rootScope', '$modal', '$route', '$http', 'CompetitionModel', function(scope, rootScope, modal, route, http, CompetitionModel) {
    rootScope.breadcrumbs = [
        { label: 'Competitions', link: 'competitions' }
    ];

    scope.currentUser = rootScope.currentUser;
    scope.competitions = [];

    http({
        method: 'GET',
        url: '/api/competition/'
    })
    .then(function(response) {
        scope.competitions = [];

        angular.forEach(response.data, function(competitionData) {
            var competition = new CompetitionModel();
            competition.set(competitionData);

            scope.competitions.push(competition);
        });
    });

    scope.getPastCompetitions = function() {
        var currentDate = new Date();
        var pastCompetitions = [];

        angular.forEach(scope.competitions, function(competition) {
            if(competition.endDate.getTime() < currentDate.getTime())
                pastCompetitions.push(competition);
        });

        pastCompetitions.sort(function(a, b) {
            return b.endDate.getTime() - a.endDate.getTime();
        });

        return pastCompetitions;
    };

    scope.getCurrentCompetitions = function() {
        var currentDate = new Date();
        var currentCompetitions = [];

        angular.forEach(scope.competitions, function(competition) {
            if(competition.startDate.getTime() <= currentDate.getTime() && competition.endDate.getTime() >= currentDate.getTime())
                currentCompetitions.push(competition);
        });

        currentCompetitions.sort(function(a, b) {
            return a.endDate.getTime() - b.endDate.getTime();
        });

        return currentCompetitions;
    };

    scope.getFutureCompetitions = function() {
        var currentDate = new Date();
        var futureCompetitions = [];

        angular.forEach(scope.competitions, function(competition) {
            if(competition.startDate.getTime() > currentDate.getTime())
                futureCompetitions.push(competition);
        });

        futureCompetitions.sort(function(a, b) {
            return a.startDate.getTime() - b.startDate.getTime();
        });

        return futureCompetitions;
    };

    scope.showCompetitionCreationModal = function() {
        var modalInstance = modal.open({
            scope: scope,
            templateUrl: 'views/competitions-modal-createCompetition.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.name = undefined;
                scope.startDate = new Date();
                scope.endDate = new Date();
                scope.errorStatusCode = undefined;

                scope.startDateOpened = false;
                scope.endDateOpened = false;

                scope.create = function() {
                    if(scope.startDate.getTime() > scope.endDate.getTime()) {
                        scope.errorStatusCode = 'END_BEFORE_START';
                        return;
                    }

                    http({
                        method: 'POST',
                        url: '/api/competition/',
                        data: {
                            name: scope.name,
                            startDate: scope.startDate.toISOString(),
                            endDate: scope.endDate.toISOString()
                        }
                    })
                    .then(function(response) {
                        modalInstance.close();
                    }, function(response) {
                        scope.errorStatusCode = response.status;
                    });
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });

        modalInstance.result.then(function() {
            route.reload();
        });
    };
}]);