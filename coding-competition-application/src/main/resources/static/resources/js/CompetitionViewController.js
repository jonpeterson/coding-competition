'use strict';

app.controller('CompetitionViewController', ['$scope', '$rootScope', '$http', '$routeParams', '$modal', 'CompetitionModel', function(scope, rootScope, http, routeParams, modal, CompetitionModel) {
    scope.currentUser = rootScope.currentUser;
    scope.currentTime = new Date().getTime();
    scope.problems = [];

    var problemConfigurations = {};
    var problemTemplates = {};

    var generateProblems = function() {
        var problems = [];
        angular.forEach(problemTemplates, function(problemTemplate) {
            var problemConfiguration = problemConfigurations[problemTemplate.id];
            problems.push({
                id: problemTemplate.id,
                description: problemTemplate.description,
                enabled: problemConfiguration ? problemConfiguration.enabled : false,
                points: problemConfiguration ? problemConfiguration.points : 0
            });
        });
        scope.problems = problems;
    };

    var competition = new CompetitionModel();
    competition.load(routeParams.id).then(function() {
        rootScope.breadcrumbs = [
            { label: 'Competitions', link: 'competitions' },
            { label: competition.name, link: '#' }
        ];

        scope.competition = competition;
        problemConfigurations = {};

        angular.forEach(competition.problemConfigurations, function(problemConfiguration) {
            problemConfigurations[problemConfiguration.problemTemplateId] = problemConfiguration;
        });

        generateProblems();
    });

    http({
        method: 'GET',
        url: '/api/problem/template/'
    })
    .then(function(response) {
        problemTemplates = {};

        angular.forEach(response.data, function(problemTemplate) {
            problemTemplates[problemTemplate.id] = problemTemplate;
        });

        generateProblems();
    });

    scope.showChangeNameModal = function() {
        modal.open({
            scope: scope,
            templateUrl: 'views/competition-modal-setName.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.newCompetitionName = competition.name;
                scope.errorStatusCode = undefined;

                scope.save = function() {
                    competition.changeName(scope.newCompetitionName).then(
                        function(response) {
                            modalInstance.close();
                        },
                        function(response) {
                            scope.errorStatusCode = response.status;
                        }
                    );
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });
    };

    scope.showChangeDatesModal = function() {
        modal.open({
            scope: scope,
            templateUrl: 'views/competition-modal-setDates.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.newCompetitionStartDate = competition.startDate;
                scope.newCompetitionEndDate = competition.endDate;
                scope.errorStatusCode = undefined;

                scope.save = function() {
                    if(scope.newCompetitionStartDate.getTime() > scope.newCompetitionEndDate.getTime()) {
                        scope.errorStatusCode = 'END_BEFORE_START';
                        return;
                    }

                    competition.changeDates(scope.newCompetitionStartDate, scope.newCompetitionEndDate).then(
                        function(response) {
                            modalInstance.close();
                        },
                        function(response) {
                            scope.errorStatusCode = response.status;
                        }
                    );
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });
    };

    scope.showAddUserModal = function() {
        modal.open({
            scope: scope,
            templateUrl: 'views/competition-modal-addUser.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.username = undefined;
                scope.errorStatusCode = undefined;

                scope.add = function() {
                    competition.addUser(scope.username).then(
                        function(response) {
                            modalInstance.close();
                        },
                        function(response) {
                            scope.errorStatusCode = response.status;
                        }
                    );
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });
    };
}]);