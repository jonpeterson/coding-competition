'use strict';

var app = angular.module('mainApp', ['ngAnimate', 'ngResource', 'ngRoute', 'ngSanitize', 'ngCookies', 'ab-base64', 'ui.bootstrap', 'angularMoment'])

    .config(['$routeProvider', '$locationProvider', '$httpProvider', 'base64', function(routeProvider, locationProvider, httpProvider, base64) {
        routeProvider
            .when('/competitions', { templateUrl: 'views/competitions.html', controller: 'CompetitionsViewController' })
            .when('/user/:username', { templateUrl: 'views/user.html', controller: 'UserViewController' })
            .when('/competition/:id', { templateUrl: 'views/competition.html', controller: 'CompetitionViewController' })
            .when('/competition/:id/user/:username', { templateUrl: 'views/competitionUser.html', controller: 'CompetitionUserViewController' })
            .when('/competition/:id/problem/:problemId', { templateUrl: 'views/competitionProblem.html', controller: 'CompetitionProblemViewController' })
            .otherwise({ redirectTo: '/competitions' });

        locationProvider.html5Mode(true);

        httpProvider.interceptors.push(['$injector', function(injector) {
            return {
                request: function(config) {
                    var rootScope = injector.get('$rootScope');
                    var cookies = injector.get('$cookies');

                    if(config.url.indexOf("/") == 0)
                        config.url = "http://thintester:8080" + config.url;

                    if(rootScope.authUsername && rootScope.authPassword) {
                        config.headers['Authorization'] = 'Basic ' + base64.encode(rootScope.authUsername + ':' + rootScope.authPassword);
                        rootScope.authUsername = undefined;
                        rootScope.authPassword = undefined;
                    } else if(cookies.authToken)
                        config.headers['X-Auth-Token'] = cookies.authToken;

                    return config;
                },

                response: function(response) {
                    var cookies = injector.get('$cookies');

                    var authToken = response.headers('X-Auth-Token');
                    if(authToken)
                        cookies.authToken = authToken;

                    return response;
                }
            };
        }]);
    }]);