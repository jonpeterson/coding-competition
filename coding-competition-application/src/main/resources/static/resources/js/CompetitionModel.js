app.factory('CompetitionModel', ['$http', function(http) {

    return function() {
        var self = this;

        self.load = function(id) {
            return http({
                method: 'GET',
                url: '/api/competition/id/' + id
            })
            .then(function(response) {
                self.set(response.data);
            });
        };

        self.set = function(data) {
            self.id = data.id;
            self.name = data.name;
            self.startDate = new Date(data.startDate);
            self.endDate = new Date(data.endDate);
            self.problemConfigurations = data.problemConfigurations;
            self.participatingUsers = data.participatingUsers;
        };

        self.changeName = function(newName) {
            return http({
                method: 'PUT',
                url: '/api/competition/id/' + self.id + '/name',
                data: newName,
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(function(response) {
                self.load(self.id);
            });
        };

        self.changeDates = function(newStartDate, newEndDate) {
            return http({
                method: 'PUT',
                url: '/api/competition/id/' + self.id + '/dates',
                data: {
                    start: newStartDate.toISOString(),
                    end: newEndDate.toISOString()
                }
            })
            .then(function(response) {
                self.load(self.id);
            });
        };

        self.configureProblem = function(problem) {
            return http({
                method: 'PUT',
                url: '/api/competition/id/' + self.id + '/problemConfiguration/problemTemplateId/' + problem.id,
                data: {
                    points: problem.points,
                    enabled: problem.enabled
                }
            })
            .then(function(response) {
                self.load(self.id);
            });
        };

        self.addUser = function(username) {
            return http({
                method: 'PUT',
                url: '/api/competition/id/' + self.id + '/participatingUser/username/' + username,
            })
            .then(function(response) {
                self.load(self.id);
            });
        };

        self.removeUser = function(username) {
            return http({
                method: 'DELETE',
                url: '/api/competition/id/' + self.id + '/participatingUser/username/' + username,
            })
            .then(function(response) {
                self.load(self.id);
            });
        };

        self.getUser = function(username) {
            for(var i in self.participatingUsers) {
                var user = self.participatingUsers[i];
                if(user.username == username)
                    return user;
            }
        };
    };
}]);