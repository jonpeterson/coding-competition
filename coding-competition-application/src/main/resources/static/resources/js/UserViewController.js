'use strict';

app.controller('UserViewController', ['$scope', '$rootScope', '$routeParams', '$modal', 'UserModel', function(scope, rootScope, routeParams, modal, UserModel) {
    scope.currentUser = rootScope.currentUser;

    var user = new UserModel();
    user.load(routeParams.username, true).then(function() {
        rootScope.breadcrumbs = [
            { label: 'Users', link: 'users' },
            { label: user.name, link: '#' }
        ];

        scope.user = user;
    });

    scope.showChangeNameModal = function() {
        var modalInstance = modal.open({
            scope: scope,
            templateUrl: 'views/user-modal-setName.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.newName = user.name;
                scope.errorStatusCode = undefined;

                scope.save = function() {
                    user.changeName(scope.newName).then(
                        function(response) {
                            modalInstance.close(user);
                        },
                        function(response) {
                            scope.errorStatusCode = response.status;
                        }
                    );
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });
    };

    scope.showChangePasswordModal = function() {
        modal.open({
            scope: scope,
            templateUrl: 'views/user-modal-setPassword.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.currentPassword = undefined;
                scope.newPassword = undefined;
                scope.confirmNewPassword = undefined;
                scope.errorStatusCode = undefined;

                scope.save = function() {
                    if(scope.newPassword != scope.confirmNewPassword) {
                        scope.errorStatusCode = "CONFIRM_DOES_NOT_MATCH"
                        return;
                    }

                    user.changePassword(scope.currentPassword, scope.newPassword).then(
                        function(response) {
                            modalInstance.close(user);
                        },
                        function(response) {
                            scope.errorStatusCode = response.status;
                        }
                    );
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });
    };

    scope.showChangeEmailModal = function() {
        var modalInstance = modal.open({
            scope: scope,
            templateUrl: 'views/user-modal-setEmail.html',
            controller: ['$scope', '$modalInstance', function(scope, modalInstance) {
                scope.newEmail = user.email;
                scope.errorStatusCode = undefined;

                scope.save = function() {
                    user.changeEmail(scope.newEmail).then(
                        function(response) {
                            modalInstance.close(user);
                        },
                        function(response) {
                            scope.errorStatusCode = response.status;
                        }
                    );
                };

                scope.cancel = function() {
                    modalInstance.dismiss();
                };

                scope.clearError = function() {
                    scope.errorStatusCode = undefined;
                };
            }]
        });
    };
}]);