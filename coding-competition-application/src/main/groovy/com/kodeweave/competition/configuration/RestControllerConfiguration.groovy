package com.kodeweave.competition.configuration

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.util.ISO8601DateFormat
import com.kodeweave.competition.controller.dto.ErrorDto
import com.kodeweave.competition.exception.AlreadyExistsException
import com.kodeweave.competition.exception.CompetitionDatesException
import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.exception.ProblemAlreadySolvedException
import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes
import org.springframework.boot.autoconfigure.web.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport

import javax.servlet.http.HttpServletResponse
import javax.validation.ConstraintViolationException

@Configuration
@ControllerAdvice
class RestControllerConfiguration extends WebMvcConfigurationSupport {

    @ExceptionHandler(NotFoundException)
    void handleNotFounds(HttpServletResponse response) {
        response.sendError(HttpStatus.NOT_FOUND.value())
    }

    @ExceptionHandler(ConstraintViolationException)
    void handleValidationBadRequests(HttpServletResponse response, ConstraintViolationException exception) {
        response.sendError(HttpStatus.BAD_REQUEST.value(), exception.constraintViolations.collect { "$it.propertyPath: $it.message" }.join(', '))
    }

    @ExceptionHandler([IllegalArgumentException, NumberFormatException])
    void handleBadRequests(HttpServletResponse response, Exception exception) {
        response.sendError(HttpStatus.BAD_REQUEST.value(), exception.message)
    }

    @ExceptionHandler([ProblemAlreadySolvedException, AlreadyExistsException, CompetitionDatesException])
    void handleConflicts(HttpServletResponse response, Exception exception) {
        response.sendError(HttpStatus.CONFLICT.value(), exception.message)
    }

    @ExceptionHandler(UnsupportedOperationException)
    void handleNotImplementeds(HttpServletResponse response, UnsupportedOperationException exception) {
        response.sendError(HttpStatus.NOT_IMPLEMENTED.value(), exception.message)
    }

    @Bean
    ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            Map<String, Object> getErrorAttributes(RequestAttributes requestAttributes, boolean includeStackTrace) {
                // get the default attributes
                def errorAttributes = super.getErrorAttributes(requestAttributes, false)

                // overrides the attributes by "whitelisting" desired attributes
                errorAttributes = [
                    status: errorAttributes['status'],
                    message: errorAttributes['message']
                ]

                // "validates" that the attributes match up with what we are publishing in the Swagger documentation
                new ErrorDto(errorAttributes)

                // return the filtered attributes
                return errorAttributes
            }
        }
    }

    @Override
    void configurePathMatch(PathMatchConfigurer pathMatchConfigurer) {
        pathMatchConfigurer.useSuffixPatternMatch = false
    }

    @Override
    void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler('/**').addResourceLocations('classpath:/static/')
    }

    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // application/json
        converters.add(new MappingJackson2HttpMessageConverter(new ObjectMapper(
            serializationInclusion: JsonInclude.Include.NON_NULL,
            dateFormat: new ISO8601DateFormat()
        )))

        // text/plain
        converters.add(new StringHttpMessageConverter())
    }
}
