package com.kodeweave.competition.configuration

import com.mangofactory.swagger.configuration.SpringSwaggerConfig
import com.mangofactory.swagger.models.dto.ApiInfo
import com.mangofactory.swagger.plugin.EnableSwagger
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableSwagger
class ApiDocumentationConfiguration {

    @Bean
    public SwaggerSpringMvcPlugin swagger(SpringSwaggerConfig springSwaggerConfig) {
        return new SwaggerSpringMvcPlugin(springSwaggerConfig)
            .apiInfo(new ApiInfo(
                'Coding Competition API',
                'blah blah blah... description here... blah blah blah',
                null,
                'jonathan.p.peterson@gmail.com',
                null,
                null
            ))
            .includePatterns('/api/.*');
    }
}
