package com.kodeweave.competition.configuration

import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint
import org.springframework.util.StringUtils
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Validator

@Configuration
class SecurityConfiguration {

    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    static class WebSecurityConfig extends WebSecurityConfigurerAdapter {

        @Autowired
        private UserDetailsService userDetailsService

        @Autowired
        private PasswordEncoder passwordEncoder

        @Override
        protected void configure(HttpSecurity httpSecurity) throws Exception {
            def authenticationEntryPoint = new BasicAuthenticationEntryPoint() {
                @Override
                void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
                    String referer = request.getHeader('Referer')
                    if(!StringUtils.isEmpty(referer)) {
                        HttpServletResponse httpResponse = (HttpServletResponse)response
                        httpResponse.addHeader('WWW-Authenticate', 'application controlled')
                        httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.message)
                    } else
                        super.commence(request, response, authException)
                }
            }
            authenticationEntryPoint.realmName = 'standard'

            // basic authentication is sufficient for username/password credentials if over HTTPS
            httpSecurity.antMatcher('/api/**').httpBasic().authenticationEntryPoint(authenticationEntryPoint)

            // don't need to protect against CSRF if the browser isn't managing the session token
            httpSecurity.csrf().disable()
        }

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
            authenticationManagerBuilder
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder)
        }
    }

    @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
        return new UserDetailsService() {
            @Override
            UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                try {
                    return userRepository.findOne(username)
                } catch(NotFoundException e) {
                    throw new UsernameNotFoundException("no user with username of '$username' exists", e)
                }
            }
        }
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder()
    }

    @Bean
    Validator validator() {
        return new LocalValidatorFactoryBean()
    }

    @Bean
    MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor()
    }
}
