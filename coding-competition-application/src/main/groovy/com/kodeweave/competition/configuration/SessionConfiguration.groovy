package com.kodeweave.competition.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.session.MapSessionRepository
import org.springframework.session.web.http.HeaderHttpSessionStrategy
import org.springframework.session.web.http.SessionRepositoryFilter

@Configuration
class SessionConfiguration {

    @Bean
    SessionRepositoryFilter sessionRepositoryFilter(@Value('${session.repository.type}') String sessionStoreType) throws IllegalArgumentException {
        switch(sessionStoreType) {
            case 'inmemory':
                def sessionRepositoryFilter = new SessionRepositoryFilter(new MapSessionRepository())
                sessionRepositoryFilter.setHttpSessionStrategy(new HeaderHttpSessionStrategy())
                return sessionRepositoryFilter

            default:
                throw new IllegalArgumentException("invalid session store type '$sessionStoreType'")
        }
    }
}
