package com.kodeweave.competition.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Configuration
class FilterConfiguration {

    @Bean
    Filter redirectFilter() {
        return new Filter() {

            @Override
            void init(FilterConfig filterConfig) throws ServletException {
            }

            @Override
            void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
                def httpRequest = (HttpServletRequest)request

                // remove the context path
                def path = httpRequest.requestURI - httpRequest.contextPath

                if(path == '/index.html' || path.startsWith('/resources/') || path.startsWith('/api/') || path.startsWith('/api-docs'))
                    chain.doFilter(request, response)
                else
                    request.getRequestDispatcher('/index.html').forward(request, response)
            }

            @Override
            void destroy() {
            }
        }
    }

    @Bean
    Filter corsFilter() {
        return new Filter() {

            @Override
            public void init(FilterConfig filterConfig) {
            }

            @Override
            public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
                ((HttpServletResponse)response).with {
                    setHeader('Access-Control-Allow-Origin', '*')
                    setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE')
                    setHeader('Access-Control-Max-Age', '3600')
                    setHeader('Access-Control-Allow-Headers', 'Content-Type, x-requested-with, Authorization, X-Auth-Token')
                    setHeader('Access-Control-Expose-Headers', 'X-Auth-Token')
                }

                chain.doFilter(request, response)
            }

            @Override
            public void destroy() {
            }
        }
    }
}
