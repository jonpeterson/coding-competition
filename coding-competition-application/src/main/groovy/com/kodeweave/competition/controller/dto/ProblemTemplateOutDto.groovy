package com.kodeweave.competition.controller.dto

import com.kodeweave.competition.problem.ProblemTemplate
import com.wordnik.swagger.annotations.ApiModelProperty

class ProblemTemplateOutDto {

    @ApiModelProperty(value = 'id of the problem template', required = true)
    String id

    @ApiModelProperty(value = 'the description of how to solve the problem', required = true)
    String description

    ProblemTemplateOutDto(ProblemTemplate template) {
        id = template.id
        description = template.description
    }
}
