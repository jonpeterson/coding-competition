package com.kodeweave.competition.controller

import com.kodeweave.competition.controller.dto.*
import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.model.Competition
import com.kodeweave.competition.model.UserProblem
import com.kodeweave.competition.repository.CompetitionRepository
import com.kodeweave.competition.repository.UserProblemRepository
import com.kodeweave.competition.repository.UserRepository
import com.wordnik.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

import javax.validation.ConstraintViolationException

@RestController
@RequestMapping('/api/competition')
@Api(value = 'competitions', description = '/api/competition')
class CompetitionController extends AbstractController {

    private static int getPointsForProblems(Map<String, Integer> pointsByTemplateId, Collection<UserProblem> problems) {
        def solvedProblems = problems.findAll { it.solved }

        if(!solvedProblems)
            return 0

        return (int)solvedProblems.sum {
            def points = pointsByTemplateId[it.problemTemplateId]
            return points ? points : 0
        }
    }


    @Autowired
    private CompetitionRepository competitionRepository

    @Autowired
    private UserRepository userRepository

    @Autowired
    private UserProblemRepository userProblemRepository

    @RequestMapping(
        method = RequestMethod.POST,
        value = '/',
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    CompetitionOutDto create(@RequestBody CreateCompetitionInDto createCompetition) throws ConstraintViolationException {
        def competititon = new Competition(
            id: UUID.randomUUID(),
            name: createCompetition.name,
            startDate: createCompetition.startDate,
            endDate: createCompetition.endDate
        )

        if(competititon.endDate.before(competititon.startDate))
            throw new IllegalArgumentException('endDate must be after startDate')

        validate(competititon)

        competitionRepository.insert(competititon)

        return new CompetitionOutDto(competititon)
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/',
        produces = MediaType.APPLICATION_JSON_VALUE)
    List<CompetitionOutDto> getAll() {
        return competitionRepository.findAll().collect { new CompetitionOutDto(it) }
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/byUsername/{username}',
        produces = MediaType.APPLICATION_JSON_VALUE)
    List<CompetitionOutDto> getAllForUsername(@PathVariable String username) throws NotFoundException {
        return competitionRepository.findAllByUsername(username).collect { new CompetitionOutDto(it) }
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/id/{id}',
        produces = MediaType.APPLICATION_JSON_VALUE)
    CompetitionOutDto get(@PathVariable UUID id) throws NotFoundException {
        return new CompetitionOutDto(competitionRepository.findOne(id))
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/id/{id}/name',
        consumes = MediaType.TEXT_PLAIN_VALUE)
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void setName(@PathVariable UUID id, @RequestBody String name) throws ConstraintViolationException, NotFoundException {
        validate(Competition, 'name', name)

        competitionRepository.updateName(id, name)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/id/{id}/dates',
        consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void setDates(@PathVariable UUID id, @RequestBody StartAndEndInDto dates) throws IllegalArgumentException, ConstraintViolationException, NotFoundException {
        if(dates.end.before(dates.start))
            throw new IllegalArgumentException('endDate must be after startDate')

        validate(Competition, 'startDate', dates.start)
        validate(Competition, 'endDate', dates.end)

        competitionRepository.updateStartDateAndEndDate(id, dates.start, dates.end)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/id/{id}/problemConfiguration/problemTemplateId/{problemTemplateId}',
        consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void setProblemConfiguration(@PathVariable UUID id, @PathVariable String problemTemplateId, @RequestBody CreateProblemConfigurationInDto createProblemConfiguration) throws ConstraintViolationException, NotFoundException {
        def problemConfiguration = new Competition.ProblemConfiguration(
            problemTemplateId: problemTemplateId,
            points: createProblemConfiguration.points,
            enabled: createProblemConfiguration.enabled
        )

        validate(problemConfiguration)

        competitionRepository.setProblemConfiguration(id, problemConfiguration)

        resetParticipatingUsersPoints(id)
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = '/id/{id}/problemConfiguration/problemTemplateId/{problemTemplateId}')
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void removeProblemConfiguration(@PathVariable UUID id, @PathVariable String problemTemplateId) throws NotFoundException {
        competitionRepository.removeProblemConfiguration(id, problemTemplateId)

        resetParticipatingUsersPoints(id)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/id/{id}/participatingUser/username/{username}')
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void updateParticipatingUser(@PathVariable UUID id, @PathVariable String username) throws ConstraintViolationException, NotFoundException {
        def user = userRepository.findOne(username)

        def participatingUser = new Competition.ParticipatingUser(
            username: username,
            name: user.name,
            points: getPointsForProblems(getPointsByTemplateId(id), userProblemRepository.findAllByCompetitionIdAndUsername(id, username)),
            active: true
        )

        validate(participatingUser)

        competitionRepository.updateParticipatingUser(id, participatingUser)
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = '/id/{id}/participatingUser/username/{username}')
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void removeParticipatingUser(@PathVariable UUID id, @PathVariable String username) throws NotFoundException {
        competitionRepository.removeParticipatingUser(id, username)
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/id/{id}/problem/byTemplateId/{problemTemplateId}',
        produces = MediaType.APPLICATION_JSON_VALUE)
    List<UserProblemOutDto> getProblemsByTemplateId(@PathVariable UUID id, @PathVariable String problemTemplateId) {
        userProblemRepository.findAllByCompetitionIdAndProblemTemplateId(id, problemTemplateId).collect { new UserProblemOutDto(it) }
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/id/{id}/problem/byUsername/{username}',
        produces = MediaType.APPLICATION_JSON_VALUE)
    List<UserProblemOutDto> getProblemsByUsername(@PathVariable UUID id, @PathVariable String username) {
        userProblemRepository.findAllByCompetitionIdAndUsername(id, username).collect { new UserProblemOutDto(it) }
    }

    private void resetParticipatingUsersPoints(UUID id) {
        def pointsByTemplateId = getPointsByTemplateId(id)

        def pointsByUsername = userProblemRepository.findAllByCompetitionId(id)

            // list<problem> -> map<username, list<problem>>
            .groupBy { it.username }

            // map<username, list<problem>> -> map<username, points>
            .collectEntries { username, problems -> [(username): getPointsForProblems(pointsByTemplateId, problems)] }

        competitionRepository.updateParticipatingUsersPoints(id, pointsByUsername)
    }

    private Map<String, Integer> getPointsByTemplateId(UUID id) {
        return competitionRepository.findOne(id).problemConfigurations.collectEntries { [(it.problemTemplateId): it.enabled ? it.points : 0] }
    }
}
