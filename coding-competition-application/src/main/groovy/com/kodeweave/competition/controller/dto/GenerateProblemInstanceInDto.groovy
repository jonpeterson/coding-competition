package com.kodeweave.competition.controller.dto

class GenerateProblemInstanceInDto {
    UUID competitionId
    String problemTemplateId
}
