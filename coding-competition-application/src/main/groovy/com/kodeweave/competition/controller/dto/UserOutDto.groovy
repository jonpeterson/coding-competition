package com.kodeweave.competition.controller.dto

import com.kodeweave.competition.model.User

class UserOutDto {
    String username
    String email
    String name
    String apiKey
    List<String> roles

    UserOutDto(User user, boolean populatePrivateFields) {
        username = user.username
        name = user.name

        if(populatePrivateFields) {
            email = user.email
            apiKey = user.apiKey
            roles = user.roles.collect { it.name() }
        }
    }
}
