package com.kodeweave.competition.controller.dto

class CreateCompetitionInDto {
    String name
    Date startDate
    Date endDate
}
