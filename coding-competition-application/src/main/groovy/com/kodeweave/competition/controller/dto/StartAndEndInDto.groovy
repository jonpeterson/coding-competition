package com.kodeweave.competition.controller.dto

class StartAndEndInDto {
    Date start
    Date end
}
