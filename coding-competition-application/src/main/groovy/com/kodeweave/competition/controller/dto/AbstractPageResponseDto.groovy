package com.kodeweave.competition.controller.dto

import com.wordnik.swagger.annotations.ApiModelProperty
import org.springframework.data.domain.Page

abstract class AbstractPageResponseDto {

    @ApiModelProperty(value = 'the total number of accounts', required = true)
    final int totalElements

    @ApiModelProperty(value = 'the total number of pages', required = true)
    final int totalPages

    @ApiModelProperty(value = 'if there is a previous page', required = true)
    final boolean hasPrevious

    @ApiModelProperty(value = 'if there is a next page', required = true)
    final boolean hasNext

    AbstractPageResponseDto(Page page) {
        totalElements = page.totalElements
        totalPages = page.totalPages
        hasPrevious = page.hasPrevious()
        hasNext = page.hasNext()
    }
}
