package com.kodeweave.competition.controller.dto

class CreateProblemConfigurationInDto {
    int points
    boolean enabled
}
