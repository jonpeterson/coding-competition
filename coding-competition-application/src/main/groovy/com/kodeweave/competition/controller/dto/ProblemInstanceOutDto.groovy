package com.kodeweave.competition.controller.dto

import com.kodeweave.competition.problem.ProblemInstance
import com.wordnik.swagger.annotations.ApiModelProperty

class ProblemInstanceOutDto {

    @ApiModelProperty(value = 'id of the problem instance', required = true)
    UUID id

    @ApiModelProperty(value = 'the template ID of this instance', required = true)
    String templateId

    @ApiModelProperty(value = 'when this expires and can no longer be answered', required = true)
    Date expireAt

    @ApiModelProperty(value = 'parameters used for answering (as per the template description)', required = true)
    List parameters

    ProblemInstanceOutDto(ProblemInstance problem) {
        id = problem.id
        templateId = problem.template.id
        expireAt = new Date(problem.expireTime)
        parameters = problem.parametersList
    }
}
