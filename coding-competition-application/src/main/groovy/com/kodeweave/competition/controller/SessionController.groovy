package com.kodeweave.competition.controller

import com.wordnik.swagger.annotations.Api
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpSession

@RestController
@RequestMapping('/api/session')
@Api(value = 'sessions', description = '/api/session')
class SessionController {

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = '/')
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void logout(HttpSession session) {
        session.invalidate();
    }
}
