package com.kodeweave.competition.controller.dto

import com.kodeweave.competition.model.Competition

class CompetitionOutDto {

    static class ProblemConfigurationOutDto {
        String problemTemplateId
        int points
        boolean enabled

        ProblemConfigurationOutDto(Competition.ProblemConfiguration problemConfiguration) {
            problemTemplateId = problemConfiguration.problemTemplateId
            points = problemConfiguration.points
            enabled = problemConfiguration.enabled
        }
    }

    static class ParticipatingUserOutDto {
        String username
        String name
        int points

        ParticipatingUserOutDto(Competition.ParticipatingUser participatingUser) {
            username = participatingUser.username
            name = participatingUser.name
            points = participatingUser.points
        }
    }

    UUID id
    String name
    Date startDate
    Date endDate
    Set<ProblemConfigurationOutDto> problemConfigurations
    Set<ParticipatingUserOutDto> participatingUsers

    CompetitionOutDto(Competition competition) {
        id = competition.id
        name = competition.name
        startDate = competition.startDate
        endDate = competition.endDate
        problemConfigurations = competition.problemConfigurations.collect { new ProblemConfigurationOutDto(it) }
        participatingUsers = competition.participatingUsers.collectMany { it.active ? [new ParticipatingUserOutDto(it)] : [] }
    }
}
