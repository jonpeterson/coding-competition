package com.kodeweave.competition.controller

import com.kodeweave.competition.controller.dto.ErrorDto
import com.kodeweave.competition.controller.dto.GenerateProblemInstanceInDto
import com.kodeweave.competition.controller.dto.ProblemInstanceOutDto
import com.kodeweave.competition.controller.dto.ProblemTemplateOutDto
import com.kodeweave.competition.exception.CompetitionDatesException
import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.exception.ProblemAlreadySolvedException
import com.kodeweave.competition.problem.ProblemService
import com.kodeweave.competition.repository.CompetitionRepository
import com.kodeweave.competition.repository.UserProblemRepository
import com.kodeweave.competition.repository.UserRepository
import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiOperation
import com.wordnik.swagger.annotations.ApiResponse
import com.wordnik.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.AccessDeniedException
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping('/api/problem')
@Api(value = 'problems', description = '/api/problem')
class ProblemController {

    @Autowired
    private ProblemService problemService

    @Autowired
    private CompetitionRepository competitionRepository

    @Autowired
    private UserRepository userRepository

    @Autowired
    private UserProblemRepository userProblemRepository

    @ApiOperation(value = 'Generates a new problem instance from the specified template', notes = 'None.')
    @ApiResponses(value = [
        @ApiResponse(code = 400, message = 'Invalid competition ID or template ID', response = ErrorDto),
        @ApiResponse(code = 403, message = 'Invalid API key', response = ErrorDto),
        @ApiResponse(code = 409, message = 'Competition hadn\'t started yet or was already over', response = ErrorDto)
    ])
    @RequestMapping(
        method = RequestMethod.POST,
        value = '/',
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    ProblemInstanceOutDto generate(@RequestParam UUID apiKey, @RequestBody GenerateProblemInstanceInDto generateProblemInstance) {
        // get user for API key
        def user
        try {
            user = userRepository.findOneByApiKey(apiKey)
        } catch(NotFoundException e) {
            throw new AccessDeniedException('invalid API key', e)
        }

        // get competition with ID
        def competition
        try {
            competition = competitionRepository.findOne(generateProblemInstance.competitionId)
        } catch(NotFoundException e) {
            throw new IllegalArgumentException('invalid competition ID', e)
        }

        def currentDate = new Date()
        if(currentDate.before(competition.startDate))
            throw new CompetitionDatesException("competition does not start until ${competition.startDate}")
        if(currentDate.after(competition.endDate))
            throw new CompetitionDatesException("competition already ended at ${competition.startDate}")

        def problemTemplateId = generateProblemInstance.problemTemplateId

        // find the problem configuration for the problemTemplateId
        def problemConfiguration = competition.problemConfigurations.find { it.problemTemplateId == problemTemplateId }

        // ensure that the problem template is enabled
        if(problemConfiguration == null || !problemConfiguration.enabled)
            throw new IllegalArgumentException("problem template '${problemTemplateId}' was not enabled on competition with id '${competition.id}'")

        try {
            return new ProblemInstanceOutDto(problemService.generate(competition.id, user.username, generateProblemInstance.problemTemplateId))
        } catch(NotFoundException e) {
            throw new IllegalArgumentException("problem template '${problemTemplateId}' was not registered")
        }
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = '/problemId/{problemId}',
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ApiOperation(value = 'Attempts to answer a generated problem', notes = 'None.')
    @ApiResponses(value = [
        @ApiResponse(code = 204, message = 'The answers were correct and the problem instance was deleted'),
        @ApiResponse(code = 400, message = 'The answers were incorrect and the problem instance was deleted', response = ErrorDto),
        @ApiResponse(code = 404, message = 'Generated problem with the provided ID was not found or expired', response = ErrorDto),
        @ApiResponse(code = 409, message = 'The problem was already solved', response = ErrorDto)
    ])
    void answer(@PathVariable UUID problemId, @RequestBody List<Object> answers) throws ProblemAlreadySolvedException {
        def problemInstance = problemService.answer(problemId, answers)

        userProblemRepository.attempt(problemInstance.competitionId, problemInstance.username, problemInstance.template.id, problemInstance.solved)

        if(!problemInstance.solved)
            throw new IllegalArgumentException("the provided answers did not match the expected answers")

        def competition
        try {
            competition = competitionRepository.findOne(problemInstance.competitionId)
        } catch(NotFoundException e) {
            // should never happen unless someone deleted the competition after the problem instance was generated
            throw new NotFoundException("competition with id '${problemInstance.competitionId}' did not exist", e)
        }

        def problemConfiguration = competition.problemConfigurations.find { it.problemTemplateId == problemInstance.template.id }
        if(!problemConfiguration)
            // should never happen unless someone deleted the competition's problem configuration after the problem instance was generated
            throw new NotFoundException("problem template '${problemInstance.template.id}' was not enabled on competition with id '${competition.id}'")

        competitionRepository.increaseParticipatingUserPoints(problemInstance.competitionId, problemInstance.username, problemConfiguration.points)
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/template',
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = 'Gets the loaded problem templates', notes = 'None.')
    Set<ProblemTemplateOutDto> listTemplates() {
        return problemService.listProblemTemplates().collect { new ProblemTemplateOutDto(it) }
    }
}
