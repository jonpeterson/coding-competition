package com.kodeweave.competition.controller.dto

import org.hibernate.validator.constraints.NotBlank

class ChangePasswordInDto {

    @NotBlank
    String currentPassword

    @NotBlank
    String newPassword
}
