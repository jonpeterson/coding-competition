package com.kodeweave.competition.controller

import com.kodeweave.competition.controller.dto.ChangePasswordInDto
import com.kodeweave.competition.controller.dto.CreateUserInDto
import com.kodeweave.competition.controller.dto.UserOutDto
import com.kodeweave.competition.exception.AlreadyExistsException
import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.model.User
import com.kodeweave.competition.repository.CompetitionRepository
import com.kodeweave.competition.repository.UserRepository
import com.wordnik.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*

import javax.servlet.http.HttpServletRequest
import javax.validation.ConstraintViolationException
import java.nio.file.AccessDeniedException

@RestController
@RequestMapping('/api/user')
@Api(value = 'users', description = '/api/user')
class UserController extends AbstractController {

    @Autowired
    private UserRepository userRepository

    @Autowired
    private CompetitionRepository competitionRepository

    @Autowired
    private PasswordEncoder passwordEncoder

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/username/{username}',
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize('isAnonymous() || principal.isAdmin()')
    UserOutDto create(@PathVariable String username, @RequestBody CreateUserInDto createUser, HttpServletRequest request) throws AccessDeniedException, ConstraintViolationException, AlreadyExistsException {
        def user = new User(
            username: username,
            email: createUser.email,
            passwordHash: passwordEncoder.encode(createUser.password),
            name: createUser.name,
            apiKey: UUID.randomUUID()
        )

        validate(user)

        // make the new user an admin if the requester is from the localhost and there are no other users currently registered
        if(request && request.remoteHost == '127.0.0.1' && userRepository.count() == 0)
            user.roles += User.Role.ADMIN

        userRepository.insert(user)

        return new UserOutDto(user, true)
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = '/username/{username}',
        produces = MediaType.APPLICATION_JSON_VALUE)
    UserOutDto get(@PathVariable String username) throws NotFoundException {
        def currentUser = getCurrentUser()
        def populatePrivateFields = currentUser && (currentUser.isAdmin() || currentUser.username == username)

        return new UserOutDto(userRepository.findOne(username), populatePrivateFields)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/username/{username}/name',
        consumes = MediaType.TEXT_PLAIN_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize('isAuthenticated() && (principal.isAdmin() || principal.username == #username)')
    void setName(@PathVariable String username, @RequestBody String name) throws AccessDeniedException, ConstraintViolationException, NotFoundException {
        validate(User, 'name', name)

        userRepository.updateName(username, name)
        competitionRepository.updateAllParticipatingUserName(username, name)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/username/{username}/email',
        consumes = MediaType.TEXT_PLAIN_VALUE)
    @PreAuthorize('isAuthenticated() && (principal.isAdmin() || principal.username == #username)')
    void setEmail(@PathVariable String username, @RequestBody String email) throws AccessDeniedException, ConstraintViolationException, NotFoundException, AlreadyExistsException {
        validate(User, 'email', email)

        userRepository.updateEmail(username, email)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/username/{username}/password',
        consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize('isAuthenticated() && principal.username == #username')
    void setPassword(@PathVariable String username, @RequestBody ChangePasswordInDto changePassword) throws AccessDeniedException, ConstraintViolationException, NotFoundException, IllegalArgumentException {
        validate(changePassword)

        def user = userRepository.findOne(username)

        // validate that old password matches the existing password
        if(!passwordEncoder.matches(changePassword.currentPassword, user.passwordHash))
            throw new IllegalArgumentException('currentPassword does not match the existing password')

        // encode the new password
        def newPasswordHash = passwordEncoder.encode(changePassword.newPassword)

        // persist the change
        userRepository.updatePasswordHash(username, newPasswordHash)
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = '/username/{username}/apiKey')
    @PreAuthorize('isAuthenticated() && (principal.isAdmin() || principal.username == #username)')
    void resetApiKey(@PathVariable String username) throws AccessDeniedException, NotFoundException, AlreadyExistsException {
        def apiKey = UUID.randomUUID()
        userRepository.updateApiKey(username, apiKey)
    }

    @RequestMapping(
        method = RequestMethod.PUT,
        value = '/username/{username}/role/{role}')
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void addRole(@PathVariable String username, @PathVariable User.Role role) throws AccessDeniedException, NotFoundException {
        userRepository.addRole(username, role)
    }

    @RequestMapping(
        method = RequestMethod.DELETE,
        value = '/username/{username}/role/{role}')
    @PreAuthorize('isAuthenticated() && principal.isAdmin()')
    void removeRole(@PathVariable String username, @PathVariable User.Role role) throws AccessDeniedException, NotFoundException {
        userRepository.removeRole(username, role)
    }
}
