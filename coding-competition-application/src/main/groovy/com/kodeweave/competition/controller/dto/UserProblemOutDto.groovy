package com.kodeweave.competition.controller.dto

import com.kodeweave.competition.model.UserProblem

class UserProblemOutDto {
    UUID competitionId
    String username
    String problemTemplateId
    boolean solved
    int failedAttempts

    UserProblemOutDto(UserProblem userProblem) {
        competitionId = userProblem.competitionId
        username = userProblem.username
        problemTemplateId = userProblem.problemTemplateId
        solved = userProblem.solved
        failedAttempts = userProblem.failedAttempts
    }
}
