package com.kodeweave.competition.controller.dto

class CreateUserInDto {
    String email
    String password
    String name
}
