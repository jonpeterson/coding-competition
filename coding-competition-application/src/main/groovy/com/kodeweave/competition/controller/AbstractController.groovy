package com.kodeweave.competition.controller

import com.kodeweave.competition.model.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder

import javax.validation.ConstraintViolationException
import javax.validation.Validator

abstract class AbstractController {

    static User getCurrentUser() {
        def principal = SecurityContextHolder.context.authentication.principal
        return principal instanceof User ? principal : null
    }


    @Autowired
    private Validator validator

    protected void validate(Object object) throws ConstraintViolationException {
        validator.validate(object).with {
            if(!empty)
                throw new ConstraintViolationException(it)
        }
    }

    protected void validate(Class<?> beanClass, String property, Object value) throws ConstraintViolationException {
        validator.validateValue(beanClass, property, value).with {
            if(!empty)
                throw new ConstraintViolationException(it)
        }
    }
}
