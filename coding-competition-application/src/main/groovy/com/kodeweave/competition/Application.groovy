package com.kodeweave.competition

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.ApplicationContext

@SpringBootApplication
class Application {
    private static ApplicationContext context

    static void main(String[] args) {
        start(args)
    }

    static void start(String... args) {
        context = new SpringApplicationBuilder(Application)
            .showBanner(false)
            .application()
            .run(args)
    }

    static void stop() {
        if(context != null) {
            SpringApplication.exit(context)
            context == null
        }
    }

    static boolean isStarted() {
        context != null
    }

    static ApplicationContext getContext() {
        context
    }
}