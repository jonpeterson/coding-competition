package com.kodeweave.competition.problem

import com.kodeweave.competition.exception.AlreadyExistsException
import com.kodeweave.competition.exception.NotFoundException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * Service that handles requests for problem generation, answering, and template registration.
 *
 * @author Jon Peterson <jonathan.p.peterson@gmail.com>
 */
@Service
class ProblemService {
    private static final Logger logger = LoggerFactory.getLogger(ProblemService)
    private final Random random = new Random()

    private final Map<String, ProblemTemplate> problemTemplates = new HashMap<String, ProblemTemplate>()
    private final Map<UUID, ProblemInstance> activeProblemsInstances = new HashMap<>()

    @Autowired
    ProblemService(Set<ProblemTemplate> problemTemplates) {
        this.problemTemplates.putAll(problemTemplates.collectEntries {
            logger.info("loading problem template '${it.id}'")

            return [(it.id): it]
        })
    }

    /**
     * @return set of all registered problem templates
     */
    Set<ProblemTemplate> listProblemTemplates() {
        synchronized(problemTemplates) {
            return problemTemplates.values()
        }
    }

    /**
     * Registers a problem template.
     *
     * @param problemTemplateId ID of the problem template that is being unregistered
     * @throws AlreadyExistsException when a problem template with the template's ID was already registered
     */
    void registerProblemTemplate(ProblemTemplate problemTemplate) throws AlreadyExistsException {
        synchronized(problemTemplates) {
            if(problemTemplates.containsKey(problemTemplate.id))
                throw new AlreadyExistsException("problem template '${problemTemplate.id}' was already registered")

            problemTemplates.put(problemTemplate.id, problemTemplate)
        }
    }

    /**
     * Unregisters a problem template.
     *
     * @param problemTemplateId ID of the problem template that is being unregistered
     * @throws NotFoundException when a problem template with the template ID could not be found
     */
    void unregisterProblemTemplate(String problemTemplateId) throws NotFoundException {
        synchronized(problemTemplates) {
            if(problemTemplates.containsKey(problemTemplateId))
                throw new NotFoundException("problem template '$problemTemplateId' was not registered")

            problemTemplates.remove(problemTemplateId)
        }
    }

    /**
     * Generates a new problem from the specified problem template for the competition ID and username.
     *
     * @param competitionId     id of competition in which this is occurring
     * @param username          username for who is generating the problem instance
     * @param problemTemplateId ID of the problem template that is being solved
     * @return the generated problem
     * @throws NotFoundException when a problem template with the template ID could not be found
     */
    ProblemInstance generate(UUID competitionId, String username, String problemTemplateId) throws NotFoundException {
        // get the problem template for the problem template ID
        def problemTemplate
        synchronized(problemTemplates) {
            problemTemplate = problemTemplates[problemTemplateId]
        }

        // ensure that the problem template exists
        if(!problemTemplate)
            throw new NotFoundException("problem template '$problemTemplateId' was not registered")

        // generate and locally store a problem instance
        def problemInstance = problemTemplate.generateInstance(competitionId, username, random)
        activeProblemsInstances.put(problemInstance.id, problemInstance)

        return problemInstance
    }

    /**
     * Attempts to solve the problem instance with the specified ID. Problems can only be answered once.
     *
     * @param problemId ID of the problem instance
     * @param answers   attempted answers for the problem
     * @return the answered problem instance
     * @throws NotFoundException when the problem instance could not be found (possibly because it expired or already
     *                           answered)
     */
    ProblemInstance answer(UUID problemId, List answers) throws NotFoundException {
        // get a locally-stored problem instance
        def problemInstance = activeProblemsInstances.remove(problemId)

        // ensure that the problem instance exists and isn't expired
        if(!problemInstance || problemInstance.isExpired())
            throw new NotFoundException("problem instance with id of '$problemId' was not found (possibly because it expired or already answered)")

        problemInstance.answer(answers)

        return problemInstance
    }
}
