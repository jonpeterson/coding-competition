package com.kodeweave.competition.model

import groovy.transform.EqualsAndHashCode
import org.hibernate.validator.constraints.NotBlank

import javax.validation.Valid
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@EqualsAndHashCode(includes = ['id'])
class Competition {

    @EqualsAndHashCode(includes = ['problemTemplateId'])
    static class ProblemConfiguration {

        @NotBlank
        String problemTemplateId

        int points = 0

        boolean enabled = true
    }

    @EqualsAndHashCode(includes = ['username'])
    static class ParticipatingUser {

        @NotBlank
        String username

        @NotBlank
        String name

        int points = 0

        Boolean active = true
    }

    @NotNull
    UUID id

    @NotBlank
    @Size(max = 255)
    String name

    @NotNull
    Date startDate

    @NotNull
    Date endDate

    @NotNull
    @Valid
    Set<ProblemConfiguration> problemConfigurations = []

    @NotNull
    @Valid
    Set<ParticipatingUser> participatingUsers = []
}
