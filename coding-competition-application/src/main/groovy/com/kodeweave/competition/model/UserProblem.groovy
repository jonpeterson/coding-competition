package com.kodeweave.competition.model

import groovy.transform.EqualsAndHashCode
import org.hibernate.validator.constraints.NotBlank

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@EqualsAndHashCode(includes = ['competitionId', 'username', 'problemTemplateId'])
class UserProblem {

    @NotNull
    UUID competitionId

    @NotBlank
    @Size(max = 20)
    String username

    @NotBlank
    String problemTemplateId

    boolean solved = false

    int failedAttempts = 0
}
