package com.kodeweave.competition.model

import groovy.transform.EqualsAndHashCode
import org.hibernate.validator.constraints.Email
import org.hibernate.validator.constraints.NotBlank
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@EqualsAndHashCode(includes = ['username'])
class User implements UserDetails {

    enum Role {
        ADMIN,
        TEST

        private GrantedAuthority authority

        Role() {
            authority = new SimpleGrantedAuthority('ROLE_' + name())
        }

        public GrantedAuthority getAuthority() {
            return authority
        }
    }

    @NotBlank
    @Pattern(regexp = '\\p{Alnum}+', message = 'must only contain letters and numbers')
    @Size(max = 20)
    String username

    @NotBlank
    @Size(max = 255)
    @Email
    String email

    @NotBlank
    @Size(min = 60, max = 60)
    String passwordHash

    @NotNull
    @Pattern(regexp = '\\p{Print}+', message = 'must only contain printable characters and spaces')
    @Size(min = 2, max = 30)
    String name

    @NotNull
    Set<Role> roles = []

    @NotNull
    UUID apiKey

    boolean isAdmin() {
        return roles.contains(Role.ADMIN)
    }

    /**
     * Should not be used accept for by Spring Security.
     *
     * @return password hash.
     * @deprecated use {@link #getPasswordHash} instead for clarity.
     */
    @Override
    @Deprecated
    String getPassword() {
        return passwordHash
    }

    /**
     * Should not be used accept for by Spring Security.
     *
     * @return list of authorities based off of {@link Role}s.
     * @deprecated use {@link #getRoles} instead for clarity.
     */
    @Override
    @Deprecated
    Collection<? extends GrantedAuthority> getAuthorities() {
        return roles.collect { it.authority }
    }

    @Override
    boolean isAccountNonExpired() {
        return true
    }

    @Override
    boolean isAccountNonLocked() {
        return true
    }

    @Override
    boolean isCredentialsNonExpired() {
        return true
    }

    @Override
    boolean isEnabled() {
        return true
    }
}
