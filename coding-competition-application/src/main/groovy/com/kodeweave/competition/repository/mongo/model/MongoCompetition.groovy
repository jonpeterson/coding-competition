package com.kodeweave.competition.repository.mongo.model

import com.kodeweave.competition.model.Competition
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = MongoCompetition.COLLECTION)
class MongoCompetition {

    static class MongoProblemConfiguration {
        static final String FIELD_PROBLEM_TEMPLATE_ID = 'problemTemplateId'
        static final String FIELD_POINTS = 'points'
        static final String FIELD_ENABLED = 'enabled'

        static MongoProblemConfiguration fromParticipatingUser(Competition.ProblemConfiguration problemConfiguration) {
            return new MongoProblemConfiguration(
                problemTemplateId: problemConfiguration.problemTemplateId,
                points: problemConfiguration.points,
                enabled: problemConfiguration.enabled
            )
        }


        @Field(MongoProblemConfiguration.FIELD_PROBLEM_TEMPLATE_ID)
        String problemTemplateId

        @Field(MongoProblemConfiguration.FIELD_POINTS)
        int points

        @Field(MongoProblemConfiguration.FIELD_ENABLED)
        boolean enabled

        Competition.ProblemConfiguration toProblemConfiguration() {
            return new Competition.ProblemConfiguration(
                problemTemplateId: problemTemplateId,
                points: points,
                enabled: enabled
            )
        }
    }

    static class MongoParticipatingUser {
        static final String FIELD_USERNAME = 'username'
        static final String FIELD_NAME = 'name'
        static final String FIELD_POINTS = 'points'
        static final String FIELD_ACTIVE = 'active'

        static MongoParticipatingUser fromParticipatingUser(Competition.ParticipatingUser participatingUser) {
            return new MongoParticipatingUser(
                username: participatingUser.username,
                name: participatingUser.name,
                points: participatingUser.points,
                active: participatingUser.active
            )
        }


        @Field(MongoParticipatingUser.FIELD_USERNAME)
        String username

        @Field(MongoParticipatingUser.FIELD_NAME)
        String name

        @Field(MongoParticipatingUser.FIELD_POINTS)
        int points

        @Field(MongoParticipatingUser.FIELD_ACTIVE)
        Boolean active

        Competition.ParticipatingUser toParticipatingUser() {
            return new Competition.ParticipatingUser(
                username: username,
                name: name,
                points: points,
                active: active
            )
        }
    }


    static final String COLLECTION = 'competitions'
    static final String FIELD_ID = '_id'
    static final String FIELD_NAME = 'name'
    static final String FIELD_START_DATE = 'startDate'
    static final String FIELD_END_DATE = 'endDate'
    static final String FIELD_PROBLEM_CONFIGURATIONS = 'problemConfigurations'
    static final String FIELD_PARTICIPATING_USERS = 'participatingUsers'
    
    static MongoCompetition fromCompetition(Competition competition) {
        return new MongoCompetition(
            id: competition.id,
            name: competition.name,
            startDate: competition.startDate,
            endDate: competition.endDate,
            problemConfigurations: competition.problemConfigurations.collectEntries { [(it.problemTemplateId): MongoProblemConfiguration.fromParticipatingUser(it)] },
            participatingUsers: competition.participatingUsers.collectEntries { [(it.username): MongoParticipatingUser.fromParticipatingUser(it)] }
        )
    }


    @Id
    @Field(MongoCompetition.FIELD_ID)
    UUID id

    @Field(MongoCompetition.FIELD_NAME)
    String name

    @Field(MongoCompetition.FIELD_START_DATE)
    Date startDate

    @Field(MongoCompetition.FIELD_END_DATE)
    Date endDate

    @Field(MongoCompetition.FIELD_PROBLEM_CONFIGURATIONS)
    Map<String, MongoProblemConfiguration> problemConfigurations

    @Field(MongoCompetition.FIELD_PARTICIPATING_USERS)
    Map<String, MongoParticipatingUser> participatingUsers

    Competition toCompetition() {
        return new Competition(
            id: id,
            name: name,
            startDate: startDate,
            endDate: endDate,
            problemConfigurations: problemConfigurations.collect { it.value.toProblemConfiguration() },
            participatingUsers: participatingUsers.collect { it.value.toParticipatingUser() }
        )
    }
}
