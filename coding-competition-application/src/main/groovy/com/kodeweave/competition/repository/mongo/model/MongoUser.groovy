package com.kodeweave.competition.repository.mongo.model

import com.kodeweave.competition.model.User
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = MongoUser.COLLECTION)
class MongoUser {
    static final String COLLECTION = 'users'
    static final String FIELD_USERNAME = '_id'
    static final String FIELD_EMAIL = 'email'
    static final String FIELD_PASSWORD_HASH = 'passwordHash'
    static final String FIELD_NAME = 'name'
    static final String FIELD_ROLES = 'roles'
    static final String FIELD_API_KEY = 'apiKey'

    static MongoUser fromUser(User user) {
        new MongoUser(
            username: user.username,
            email: user.email,
            passwordHash: user.passwordHash,
            name: user.name,
            roles: user.roles.collect { it.name() },
            apiKey: user.apiKey
        )
    }


    @Id
    @Field(MongoUser.FIELD_USERNAME)
    String username

    @Field(MongoUser.FIELD_EMAIL)
    @Indexed(unique = true)
    String email

    @Field(MongoUser.FIELD_PASSWORD_HASH)
    String passwordHash

    @Field(MongoUser.FIELD_NAME)
    String name

    @Field(MongoUser.FIELD_ROLES)
    List<String> roles

    @Field(MongoUser.FIELD_API_KEY)
    @Indexed(unique = true)
    UUID apiKey

    User toUser() {
        return new User(
            username: username,
            email: email,
            passwordHash: passwordHash,
            name: name,
            roles: roles.collect { User.Role.valueOf(it) },
            apiKey: apiKey
        )
    }
}
