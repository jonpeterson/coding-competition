package com.kodeweave.competition.repository.mongo.model

import com.kodeweave.competition.model.UserProblem
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.CompoundIndex
import org.springframework.data.mongodb.core.index.CompoundIndexes
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field

@Document(collection = MongoUserProblem.COLLECTION)
@CompoundIndexes([
    @CompoundIndex(def = '{"competitionId": 1, "username": 1, "problemTemplateId": 1}', unique = true)
])
class MongoUserProblem {
    static final String COLLECTION = 'userProblems'
    static final String FIELD_ID = '_id'
    static final String FIELD_COMPETITION_ID = 'competitionId'
    static final String FIELD_USERNAME = 'username'
    static final String FIELD_PROBLEM_TEMPLATE_ID = 'problemTemplateId'
    static final String FIELD_SOLVED = 'solved'
    static final String FIELD_FAILED_ATTEMPTS = 'failedAttempts'


    @Id
    @Field(MongoUserProblem.FIELD_ID)
    UUID id

    @Field(MongoUserProblem.FIELD_COMPETITION_ID)
    @Indexed
    UUID competitionId

    @Field(MongoUserProblem.FIELD_USERNAME)
    @Indexed
    String username

    @Field(MongoUserProblem.FIELD_PROBLEM_TEMPLATE_ID)
    @Indexed
    String problemTemplateId

    @Field(MongoUserProblem.FIELD_SOLVED)
    boolean solved = false

    @Field(MongoUserProblem.FIELD_FAILED_ATTEMPTS)
    int failedAttempts = 0

    UserProblem toUserProblem() {
        return new UserProblem(
            competitionId: competitionId,
            username: username,
            problemTemplateId: problemTemplateId,
            solved: solved,
            failedAttempts: failedAttempts
        )
    }
}
