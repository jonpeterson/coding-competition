package com.kodeweave.competition.repository

import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.model.Competition
import com.kodeweave.competition.model.Competition.ParticipatingUser
import com.kodeweave.competition.model.Competition.ProblemConfiguration

interface CompetitionRepository {

    void insert(Competition competition)

    List<Competition> findAll()

    List<Competition> findAllByUsername(String username)

    Competition findOne(UUID id) throws NotFoundException

    void updateName(UUID id, String name) throws NotFoundException

    void updateStartDateAndEndDate(UUID id, Date startDate, Date endDate) throws NotFoundException

    void setProblemConfiguration(UUID id, ProblemConfiguration problemConfiguration) throws NotFoundException

    void removeProblemConfiguration(UUID id, String problemTemplateId) throws NotFoundException

    void updateParticipatingUser(UUID id, ParticipatingUser participatingUser) throws NotFoundException

    void removeParticipatingUser(UUID id, String username) throws NotFoundException

    void increaseParticipatingUserPoints(UUID id, String username, int points) throws NotFoundException

    void updateAllParticipatingUserName(String username, String name)

    void updateParticipatingUsersPoints(UUID id, Map<String, Integer> pointsByUsername)
}
