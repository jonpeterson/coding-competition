package com.kodeweave.competition.repository.mongo

import com.kodeweave.competition.exception.ProblemAlreadySolvedException
import com.kodeweave.competition.model.UserProblem
import com.kodeweave.competition.repository.UserProblemRepository
import com.kodeweave.competition.repository.mongo.model.MongoUserProblem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository

@Repository
class MongoUserProblemRepository extends AbstractMongoRepository implements UserProblemRepository {

    @Autowired
    MongoTemplate mongoTemplate

    @Override
    Collection<UserProblem> findAllByCompetitionId(UUID competitionId) {
        return mongoTemplate.find(
            new Query()
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_COMPETITION_ID).is(competitionId)),
            MongoUserProblem
        ).collect { it.toUserProblem() }
    }

    @Override
    Collection<UserProblem> findAllByCompetitionIdAndUsername(UUID competitionId, String username) {
        return mongoTemplate.find(
            new Query()
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_COMPETITION_ID).is(competitionId))
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_USERNAME).is(username)),
            MongoUserProblem
        ).collect { it.toUserProblem() }
    }

    @Override
    Collection<UserProblem> findAllByCompetitionIdAndProblemTemplateId(UUID competitionId, String problemTemplateId) {
        return mongoTemplate.find(
            new Query()
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_COMPETITION_ID).is(competitionId))
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_PROBLEM_TEMPLATE_ID).is(problemTemplateId)),
            MongoUserProblem
        ).collect { it.toUserProblem() }
    }

    @Override
    void attempt(UUID competitionId, String username, String problemTemplateId, boolean solved) throws ProblemAlreadySolvedException {
        def failedAttemptsIncrement = solved ? 0 : 1

        def writeResult = mongoTemplate.updateFirst(
            new Query()
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_COMPETITION_ID).is(competitionId))
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_USERNAME).is(username))
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_PROBLEM_TEMPLATE_ID).is(problemTemplateId))
                .addCriteria(Criteria.where(MongoUserProblem.FIELD_SOLVED).is(false)),
            new Update()
                .set(MongoUserProblem.FIELD_SOLVED, solved)
                .inc(MongoUserProblem.FIELD_FAILED_ATTEMPTS, failedAttemptsIncrement),
            MongoUserProblem
        )

        if(writeResult.n < 1)
            try {
                mongoTemplate.insert(new MongoUserProblem(
                    id: UUID.randomUUID(),
                    competitionId: competitionId,
                    username: username,
                    problemTemplateId: problemTemplateId,
                    solved: solved,
                    failedAttempts: failedAttemptsIncrement
                ))
            } catch(DataIntegrityViolationException e) {
                throw new ProblemAlreadySolvedException('this competition/username/problem combination has already been solved', e)
            }
    }
}
