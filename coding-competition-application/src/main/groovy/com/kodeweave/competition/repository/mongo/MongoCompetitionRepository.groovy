package com.kodeweave.competition.repository.mongo

import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.model.Competition
import com.kodeweave.competition.model.Competition.ParticipatingUser
import com.kodeweave.competition.model.Competition.ProblemConfiguration
import com.kodeweave.competition.repository.CompetitionRepository
import com.kodeweave.competition.repository.mongo.model.MongoCompetition
import com.kodeweave.competition.repository.mongo.model.MongoCompetition.MongoParticipatingUser
import com.kodeweave.competition.repository.mongo.model.MongoCompetition.MongoProblemConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository

@Repository
class MongoCompetitionRepository extends AbstractMongoRepository implements CompetitionRepository {

    @Autowired
    MongoTemplate mongoTemplate

    @Override
    void insert(Competition competition) {
        mongoTemplate.insert(MongoCompetition.fromCompetition(competition))
    }

    @Override
    List<Competition> findAll() {
        return mongoTemplate.findAll(MongoCompetition).collect { it.toCompetition() }
    }

    @Override
    List<Competition> findAllByUsername(String username) {
        return mongoTemplate.find(
            new Query(Criteria.where("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}").exists(true)),
            MongoCompetition
        ).collect { it.toCompetition() }
    }

    @Override
    Competition findOne(UUID id) throws NotFoundException {
        def mongoCompetition = mongoTemplate.findOne(
            new Query(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
            MongoCompetition
        )

        if(!mongoCompetition)
            throw new NotFoundException("no competition found with id of '$id'")

        return mongoCompetition.toCompetition()
    }

    @Override
    void updateName(UUID id, String name) throws NotFoundException {
        ensureUpdate("no competition found with id of '$id'") {
            mongoTemplate.updateFirst(
                new Query()
                    .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                new Update()
                    .set(MongoCompetition.FIELD_NAME, name),
                MongoCompetition
            )
        }
    }

    @Override
    void updateStartDateAndEndDate(UUID id, Date startDate, Date endDate) throws NotFoundException {
        ensureUpdate("no competition found with id of '$id'") {
            mongoTemplate.updateFirst(
                new Query()
                    .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                new Update()
                    .set(MongoCompetition.FIELD_START_DATE, startDate)
                    .set(MongoCompetition.FIELD_END_DATE, endDate),
                MongoCompetition
            )
        }
    }

    @Override
    void setProblemConfiguration(UUID id, ProblemConfiguration problemConfiguration) throws NotFoundException {
        ensureUpdate("no competition found with id of '$id'") {
            mongoTemplate.updateFirst(
                new Query()
                    .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                new Update()
                    .set("${MongoCompetition.FIELD_PROBLEM_CONFIGURATIONS}.${problemConfiguration.problemTemplateId}", MongoProblemConfiguration.fromParticipatingUser(problemConfiguration)),
                MongoCompetition
            )
        }
    }

    @Override
    void removeProblemConfiguration(UUID id, String problemTemplateId) throws NotFoundException {
        ensureUpdate("no competition found with id of '$id' or didn't contain problem configuration with id of '$problemTemplateId'") {
            mongoTemplate.updateFirst(
                new Query()
                    .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                new Update()
                    .unset("${MongoCompetition.FIELD_PROBLEM_CONFIGURATIONS}.${problemTemplateId}"),
                MongoCompetition
            )
        }
    }

    @Override
    void updateParticipatingUser(UUID id, ParticipatingUser participatingUser) throws NotFoundException {
        try {
            ensureUpdate("no competition found with id of '$id'") {
                mongoTemplate.updateFirst(
                    new Query()
                        .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                    new Update()
                        .set("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${participatingUser.username}", MongoParticipatingUser.fromParticipatingUser(participatingUser)),
                    MongoCompetition
                )
            }
        } catch(NotFoundException e) {
            throw e
        }
    }

    @Override
    void removeParticipatingUser(UUID id, String username) throws NotFoundException {
        ensureUpdate("no competition found with id of '$id' or didn't contain participating user with username of '$username'") {
            mongoTemplate.updateFirst(
                new Query()
                    .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id))
                    .addCriteria(Criteria.where("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}").exists(true)),
                new Update()
                    .unset("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}"),
                MongoCompetition
            )
        }
    }

    @Override
    void increaseParticipatingUserPoints(UUID id, String username, int points) throws NotFoundException {
        ensureUpdate("no competition found with id of '$id' or didn't contain participating user with username of '$username'") {
            mongoTemplate.updateFirst(
                new Query()
                    .addCriteria(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                new Update()
                    .inc("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}.${MongoParticipatingUser.FIELD_POINTS}", points),
                MongoCompetition
            )
        }
    }

    @Override
    void updateAllParticipatingUserName(String username, String name) {
        mongoTemplate.updateMulti(
            new Query()
                .addCriteria(Criteria.where("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}").exists(true)),
            new Update()
                .set("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}.${MongoParticipatingUser.FIELD_NAME}", name),
            MongoCompetition
        )
    }

    @Override
    void updateParticipatingUsersPoints(UUID id, Map<String, Integer> pointsByUsername) {
        def update = new Update()

        pointsByUsername.each { username, points ->
            update.set("${MongoCompetition.FIELD_PARTICIPATING_USERS}.${username}.${MongoParticipatingUser.FIELD_POINTS}", points)
        }

        if(update.updateObject.toMap().size() > 0)
            mongoTemplate.updateFirst(
                new Query(Criteria.where(MongoCompetition.FIELD_ID).is(id)),
                update,
                MongoCompetition
            )
    }
}
