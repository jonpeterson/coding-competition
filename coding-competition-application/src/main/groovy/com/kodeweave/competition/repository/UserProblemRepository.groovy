package com.kodeweave.competition.repository

import com.kodeweave.competition.exception.ProblemAlreadySolvedException
import com.kodeweave.competition.model.UserProblem

interface UserProblemRepository {

    Collection<UserProblem> findAllByCompetitionId(UUID competitionId)

    Collection<UserProblem> findAllByCompetitionIdAndUsername(UUID competitionId, String username)

    Collection<UserProblem> findAllByCompetitionIdAndProblemTemplateId(UUID competitionId, String problemTemplateId)

    void attempt(UUID competitionId, String username, String problemTemplateId, boolean solved) throws ProblemAlreadySolvedException
}
