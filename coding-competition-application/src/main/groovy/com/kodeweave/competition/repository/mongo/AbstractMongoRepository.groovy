package com.kodeweave.competition.repository.mongo

import com.kodeweave.competition.exception.NotFoundException
import com.mongodb.WriteResult

abstract class AbstractMongoRepository {

    protected static void ensureUpdate(String notFoundMessage, Closure<WriteResult> writeOperation) throws NotFoundException {
        if(writeOperation.call().n < 1)
            throw new NotFoundException(notFoundMessage)
    }
}
