package com.kodeweave.competition.repository

import com.kodeweave.competition.exception.AlreadyExistsException
import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.model.User

interface UserRepository {

    void insert(User user) throws AlreadyExistsException

    int count()

    User findOne(String username) throws NotFoundException

    User findOneByApiKey(UUID apiKey) throws NotFoundException

    void updateName(String username, String name) throws NotFoundException

    void updateEmail(String username, String email) throws NotFoundException, AlreadyExistsException

    void updatePasswordHash(String username, String passwordHash) throws NotFoundException

    void updateApiKey(String username, UUID apiKey) throws NotFoundException, AlreadyExistsException

    void addRole(String username, User.Role role) throws NotFoundException

    void removeRole(String username, User.Role role) throws NotFoundException
}
