package com.kodeweave.competition.repository.mongo

import com.kodeweave.competition.exception.AlreadyExistsException
import com.kodeweave.competition.exception.NotFoundException
import com.kodeweave.competition.model.User
import com.kodeweave.competition.repository.UserRepository
import com.kodeweave.competition.repository.mongo.model.MongoUser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.stereotype.Repository

@Repository
class MongoUserRepository extends AbstractMongoRepository implements UserRepository {

    @Autowired
    MongoTemplate mongoTemplate

    @Override
    void insert(User user) throws AlreadyExistsException {
        try {
            mongoTemplate.insert(MongoUser.fromUser(user))
        } catch(DataIntegrityViolationException e) {
            throw new AlreadyExistsException('username or email already being used by another user', e)
        }
    }

    int count() {
        return mongoTemplate.count(new Query(), MongoUser)
    }

    @Override
    User findOne(String username) throws NotFoundException {
        def mongoUser = mongoTemplate.findOne(
            new Query(Criteria.where(MongoUser.FIELD_USERNAME).is(username)),
            MongoUser
        )

        if(!mongoUser)
            throw new NotFoundException("no user found with username of '$username'")

        return mongoUser.toUser()
    }

    @Override
    User findOneByApiKey(UUID apiKey) throws NotFoundException {
        def mongoUser = mongoTemplate.findOne(
            new Query(Criteria.where(MongoUser.FIELD_API_KEY).is(apiKey)),
            MongoUser
        )

        if(!mongoUser)
            throw new NotFoundException("no user found with api key of '$apiKey'")

        return mongoUser.toUser()
    }

    private void updateFirst(String username, Update update) throws NotFoundException {
        ensureUpdate("no user found with username of '$username'") {
            mongoTemplate.updateFirst(
                new Query(Criteria.where(MongoUser.FIELD_USERNAME).is(username)),
                update,
                MongoUser
            )
        }
    }

    private void updateFirst(String username, String mongoField, Object value, String alreadyExistsMessage = null) throws NotFoundException, DataIntegrityViolationException {
        try {
            updateFirst(username, new Update().set(mongoField, value))
        } catch(DataIntegrityViolationException e) {
            throw new AlreadyExistsException(alreadyExistsMessage ?: "$mongoField already being used by another user", e)
        }
    }

    @Override
    void updateName(String username, String name) throws NotFoundException {
        updateFirst(username, MongoUser.FIELD_NAME, name)
    }

    @Override
    void updateEmail(String username, String email) throws NotFoundException, AlreadyExistsException {
        updateFirst(username, MongoUser.FIELD_EMAIL, email)
    }

    @Override
    void updatePasswordHash(String username, String passwordHash) throws NotFoundException {
        updateFirst(username, MongoUser.FIELD_PASSWORD_HASH, passwordHash)
    }

    @Override
    void updateApiKey(String username, UUID apiKey) throws NotFoundException, AlreadyExistsException {
        updateFirst(username, MongoUser.FIELD_API_KEY, apiKey)
    }

    @Override
    void addRole(String username, User.Role role) throws NotFoundException {
        updateFirst(username, new Update().addToSet(MongoUser.FIELD_ROLES, role.name()))
    }

    @Override
    void removeRole(String username, User.Role role) throws NotFoundException {
        updateFirst(username, new Update().pull(MongoUser.FIELD_ROLES, role.name()))
    }
}
