package com.kodeweave.competition.validation

import org.springframework.data.domain.PageRequest

import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import java.lang.annotation.Retention
import java.lang.annotation.Target

import static java.lang.annotation.ElementType.PARAMETER
import static java.lang.annotation.RetentionPolicy.RUNTIME

@Target([PARAMETER])
@Retention(RUNTIME)
@Constraint(validatedBy = Validator)
public @interface PageRequestValid {

    static class Validator implements ConstraintValidator<PageRequestValid, PageRequest> {
        private int maxPageSize
        private String[] sortableFields

        @Override
        void initialize(PageRequestValid constraintAnnotation) {
            maxPageSize = constraintAnnotation.maxPageSize()
            sortableFields = constraintAnnotation.sortableFields()
        }

        @Override
        boolean isValid(PageRequest value, ConstraintValidatorContext context) {
            return value.pageNumber >= 0 &&
                   value.pageSize >= 0 &&
                   value.pageSize < maxPageSize &&
                   value.sort.every { sortableFields.contains(it.property) }
        }
    }


    String message() default 'invalid page request parameters'

    Class<?>[] groups() default []

    Class<? extends Payload>[] payload() default []

    int maxPageSize() default 100

    String[] sortableFields() default []
}
