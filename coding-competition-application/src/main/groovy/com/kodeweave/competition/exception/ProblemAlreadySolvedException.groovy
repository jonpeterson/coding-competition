package com.kodeweave.competition.exception

class ProblemAlreadySolvedException extends Exception {

    ProblemAlreadySolvedException(String message = null, Exception cause = null) {
        super(message, cause)
    }
}
