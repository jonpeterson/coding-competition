package com.kodeweave.competition.exception

class AlreadyExistsException extends Exception {

    AlreadyExistsException(String message = null, Exception cause = null) {
        super(message, cause)
    }
}
