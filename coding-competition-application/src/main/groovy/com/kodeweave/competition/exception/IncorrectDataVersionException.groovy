package com.kodeweave.competition.exception

class IncorrectDataVersionException extends Exception {

    IncorrectDataVersionException(String message = null, Exception cause = null) {
        super(message, cause)
    }
}
