package com.kodeweave.competition.exception

class CompetitionDatesException extends Exception {

    CompetitionDatesException(String message = null, Exception cause = null) {
        super(message, cause)
    }
}
