package com.kodeweave.competition.exception

class NotFoundException extends Exception {

    NotFoundException(String message = null, Exception cause = null) {
        super(message, cause)
    }
}
