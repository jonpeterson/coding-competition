package com.kodeweave.competition.controller.dto;

import com.wordnik.swagger.annotations.ApiModel;
import com.wordnik.swagger.annotations.ApiModelProperty;

import java.util.HashMap;

/**
 * This is purely for Swagger documentation. This describes the body format of any 4xx or 5xx HTTP response. Written in
 * Java (instead of Groovy) so that Swagger won't pick up the MetaClass during reflection.
 */
@ApiModel
public class ErrorDto extends HashMap<String, Object> {

    @ApiModelProperty(value = "HTTP response status code 4xx or 5xx", required = true)
    public int status;

    @ApiModelProperty(value = "message describing why the error occurred", required = true)
    public String message;
}
