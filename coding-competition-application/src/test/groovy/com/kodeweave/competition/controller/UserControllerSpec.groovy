package com.kodeweave.competition.controller

import static org.apache.http.HttpStatus.*
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE

class UserControllerSpec extends AbstractControllerSpec {

    def 'tests'() {
        expect:
        def zackClient = createClient('zack', 'zyx')

        // create user using credentials for the yet-to-be-created user
        zackClient.put(
            path: '/api/user/username/zack',
            body: [
                email: 'zack.attack@test.com',
                password: 'zyx',
                name: 'Zack'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_UNAUTHORIZED

        // create user with a invalid field
        unauthenticatedClient.put(
            path: '/api/user/username/zack',
            body: [
                email: 'notAnEmail',
                password: 'zyx',
                name: 'Zack'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_BAD_REQUEST

        // create user with a missing field
        unauthenticatedClient.put(
            path: '/api/user/username/zack',
            body: [
                password: 'zyx',
                name: 'Zack'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_BAD_REQUEST

        // create user successfully
        with(unauthenticatedClient.put(
            path: '/api/user/username/zack',
            body: [
                email: 'zack.attack@test.com',
                password: 'zyx',
                name: 'Zack'
            ],
            contentType: APPLICATION_JSON_VALUE
        )) {
            status == SC_OK
            with(responseData) {
                size() == 5
                username == 'zack'
                email == 'zack.attack@test.com'
                name == 'Zack'
                apiKey != null
                roles == []
            }
        }

        // create user that already exists
        unauthenticatedClient.put(
            path: '/api/user/username/zack',
            body: [
                email: 'zack.attack@test.com',
                password: 'zyx',
                name: 'Zack'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_CONFLICT

        // get created user without credentials
        with(unauthenticatedClient.get(path: '/api/user/username/zack')) {
            status == SC_OK
            with(responseData) {
                size() == 2
                username == 'zack'
                name == 'Zack'
            }
        }

        // get created user with the user's credentials
        with(zackClient.get(path: '/api/user/username/zack')) {
            status == SC_OK
            with(responseData) {
                size() == 5
                username == 'zack'
                email == 'zack.attack@test.com'
                name == 'Zack'
                apiKey != null
                roles == []
            }
        }

        // get created user with admin's credentials
        with(adminClient.get(path: '/api/user/username/zack')) {
            status == SC_OK
            with(responseData) {
                size() == 5
                username == 'zack'
                email == 'zack.attack@test.com'
                name == 'Zack'
                apiKey != null
                roles == []
            }
        }

        // get non-existent user with admin's credentials
        adminClient.get(path: '/api/user/username/blah').status == SC_NOT_FOUND

        // set user's name and email with user's credentials
        zackClient.put(
            path: '/api/user/username/zack/name',
            body: 'Not Zack',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_OK
        zackClient.put(
            path: '/api/user/username/zack/email',
            body: 'blah@test.com',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_OK

        // get updated user with the user's credentials
        with(zackClient.get(path: '/api/user/username/zack').responseData) {
            name == 'Not Zack'
            email == 'blah@test.com'
        }

        // set user's name and email with admin's credentials
        adminClient.put(
            path: '/api/user/username/zack/name',
            body: 'Zack',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_OK
        adminClient.put(
            path: '/api/user/username/zack/email',
            body: 'zack.attack@test.com',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_OK

        // get updated user with the user's credentials
        with(zackClient.get(path: '/api/user/username/zack').responseData) {
            name == 'Zack'
            email == 'zack.attack@test.com'
        }

        // set user's name and email with another user's credentials
        unauthenticatedClient.put(
            path: '/api/user/username/zack/name',
            body: 'Not Zack',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_UNAUTHORIZED
        unauthenticatedClient.put(
            path: '/api/user/username/zack/email',
            body: 'blah@test.com',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_UNAUTHORIZED

        // set user's name and email with another user's credentials
        userClient.put(
            path: '/api/user/username/zack/name',
            body: 'Not Zack',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_FORBIDDEN
        userClient.put(
            path: '/api/user/username/zack/email',
            body: 'blah@test.com',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_FORBIDDEN

        // set user's name and email to invalid value with user's credentials
        zackClient.put(
            path: '/api/user/username/zack/name',
            body: '',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_BAD_REQUEST
        zackClient.put(
            path: '/api/user/username/zack/email',
            body: 'blah',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_BAD_REQUEST

        // set user's email to already used email with user's credentials
        zackClient.put(
            path: '/api/user/username/zack/email',
            body: 'user@test.com',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_CONFLICT

        // get updated user with the user's credentials
        with(zackClient.get(path: '/api/user/username/zack').responseData) {
            name == 'Zack'
            email == 'zack.attack@test.com'
        }

        // set user's password with user's credentials
        zackClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'zyx',
                newPassword: 'xyz'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_OK

        // set user's password with user's now-invalid credentials
        zackClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'xyz',
                newPassword: 'zyx'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_UNAUTHORIZED

        // set user's password with admin's credentials
        adminClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'xyz',
                newPassword: 'zyx'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_FORBIDDEN

        // set user's password with admin's and another user's credentials
        adminClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'xyz',
                newPassword: 'zyx'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_FORBIDDEN
        unauthenticatedClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'xyz',
                newPassword: 'zyx'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_UNAUTHORIZED
        userClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'xyz',
                newPassword: 'zyx'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_FORBIDDEN

        // set user's password with user's credentials
        createClient('zack', 'xyz').put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'xyz',
                newPassword: 'zyx'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_OK

        // set user's invalid password with user's credentials
        zackClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'zyx',
                newPassword: ''
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_BAD_REQUEST
        zackClient.put(
            path: '/api/user/username/zack/password',
            body: [
                currentPassword: 'blah',
                newPassword: 'xyz'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_BAD_REQUEST

        // get user's API key with the user's credentials
        def oldApiKey = zackClient.get(path: '/api/user/username/zack').responseData.apiKey

        // reset user's API key with user's credentials
        zackClient.delete(path: '/api/user/username/zack/apiKey').status == SC_OK

        def newApiKey = zackClient.get(path: '/api/user/username/zack').responseData.apiKey
        newApiKey != oldApiKey

        // reset user's API key with admin's and other user's credentials
        adminClient.delete(path: '/api/user/username/zack/apiKey').status == SC_OK
        unauthenticatedClient.delete(path: '/api/user/username/zack/apiKey').status == SC_UNAUTHORIZED
        userClient.delete(path: '/api/user/username/zack/apiKey').status == SC_FORBIDDEN

        // add and remove role with user's and other user's credentials
        zackClient.put(path: '/api/user/username/zack/role/ADMIN').status == SC_FORBIDDEN
        zackClient.delete(path: '/api/user/username/zack/role/ADMIN').status == SC_FORBIDDEN
        userClient.put(path: '/api/user/username/zack/role/ADMIN').status == SC_FORBIDDEN
        userClient.delete(path: '/api/user/username/zack/role/ADMIN').status == SC_FORBIDDEN
        unauthenticatedClient.put(path: '/api/user/username/zack/role/ADMIN').status == SC_UNAUTHORIZED
        unauthenticatedClient.delete(path: '/api/user/username/zack/role/ADMIN').status == SC_UNAUTHORIZED

        // add and remove role with admin's credentials
        adminClient.put(path: '/api/user/username/zack/role/ADMIN').status == SC_OK
        zackClient.delete(path: '/api/user/username/zack/role/ADMIN').status == SC_OK
        zackClient.delete(path: '/api/user/username/zack/role/ADMIN').status == SC_FORBIDDEN

        // add and remove invalid role with admin's credentials
        adminClient.put(path: '/api/user/username/zack/role/BLAH').status == SC_BAD_REQUEST
        adminClient.delete(path: '/api/user/username/zack/role/BLAH').status == SC_BAD_REQUEST
    }
}
