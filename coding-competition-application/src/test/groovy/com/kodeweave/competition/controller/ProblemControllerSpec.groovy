package com.kodeweave.competition.controller

import com.kodeweave.competition.problem.DummyProblemTemplate

import java.text.SimpleDateFormat

import static org.apache.http.HttpStatus.*
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE

class ProblemControllerSpec extends AbstractControllerSpec {

    def 'tests'() {
        setup:

        def currentDate = new Date()
        def pastDateString = new SimpleDateFormat('yyyy-MM-dd\'T\'HH:mm\'Z\'').format(new Date(currentDate.time - 86400000))
        def futureDateString = new SimpleDateFormat('yyyy-MM-dd\'T\'HH:mm\'Z\'').format(new Date(currentDate.time + 86400000))

        def competitionId = adminClient.post(
            path: '/api/competition/',
            body: [
                name: 'Competition A',
                startDate: futureDateString,
                endDate: futureDateString
            ],
            contentType: APPLICATION_JSON_VALUE
        ).responseData.id

        adminClient.put(
            path: "/api/competition/id/${competitionId}/problemConfiguration/problemTemplateId/dummy",
            body: [
                points: 100,
                enabled: true
            ],
            contentType: APPLICATION_JSON_VALUE
        )

        adminClient.put(path: "/api/competition/id/${competitionId}/participatingUser/username/${USER_USERNAME}")

        def apiKey = adminClient.get(path: "/api/user/username/${USER_USERNAME}").responseData.apiKey


        expect:

        with(userClient.get(path: '/api/problem/template')) {
            status == SC_OK
            with(responseData) {
                size() == 1

                with(it[0]) {
                    id == 'dummy'
                    description != null
                }
            }
        }

        // check user problems
        with(userClient.get(path: "/api/competition/id/${competitionId}/problem/byUsername/${USER_USERNAME}")) {
            status == SC_OK
            responseData.size() == 0
        }

        // generate problem but fail due to already over
        userClient.post(
            path: '/api/problem/',
            query: [
                apiKey: apiKey
            ],
            body: [
                competitionId: competitionId,
                problemTemplateId: 'dummy'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_CONFLICT

        // set dates
        adminClient.put(
            path: "/api/competition/id/${competitionId}/dates",
            body: [
                start: pastDateString,
                end: pastDateString
            ],
            contentType: APPLICATION_JSON_VALUE
        )

        // generate problem but fail due to not started yet
        userClient.post(
            path: '/api/problem/',
            query: [
                apiKey: apiKey
            ],
            body: [
                competitionId: competitionId,
                problemTemplateId: 'dummy'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_CONFLICT

        // set dates
        adminClient.put(
            path: "/api/competition/id/${competitionId}/dates",
            body: [
                start: pastDateString,
                end: futureDateString
            ],
            contentType: APPLICATION_JSON_VALUE
        )

        // generate problem
        def generatedProblemResponse1 = userClient.post(
            path: '/api/problem/',
            query: [
                apiKey: apiKey
            ],
            body: [
                competitionId: competitionId,
                problemTemplateId: 'dummy'
            ],
            contentType: APPLICATION_JSON_VALUE
        )
        with(generatedProblemResponse1) {
            status == SC_OK
            with(responseData) {
                id != null
                templateId == 'dummy'
                expireAt != null
                parameters == [DummyProblemTemplate.PARAMETER]
            }
        }
        def problemId1 = generatedProblemResponse1.responseData.id

        // answer incorrectly
        with(userClient.delete(
            path: "/api/problem/problemId/$problemId1",
            body: [DummyProblemTemplate.EXPECTED_ANSWER + 'no'],
            contentType: APPLICATION_JSON_VALUE
        )) {
            status == SC_BAD_REQUEST
        }

        // answer correctly, but the problem instance is gone
        with(userClient.delete(
            path: "/api/problem/problemId/$problemId1",
            body: [DummyProblemTemplate.EXPECTED_ANSWER],
            contentType: APPLICATION_JSON_VALUE
        )) {
            status == SC_NOT_FOUND
        }

        // check user problems
        with(userClient.get(path: "/api/competition/id/${competitionId}/problem/byUsername/${USER_USERNAME}")) {
            status == SC_OK
            with(responseData) {
                size() == 1

                with(it[0]) {
                    competitionId == competitionId
                    failedAttempts == 1
                    problemTemplateId == 'dummy'
                    !solved
                    username == AbstractControllerSpec.USER_USERNAME
                }
            }
        }

        // generate problem
        def problemId2 = userClient.post(
            path: '/api/problem/',
            query: [
                apiKey: apiKey
            ],
            body: [
                competitionId: competitionId,
                problemTemplateId: 'dummy'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).responseData.id

        // answer correctly, but after expiration
        sleep(2000)
        with(userClient.delete(
            path: "/api/problem/problemId/$problemId2",
            body: [DummyProblemTemplate.EXPECTED_ANSWER],
            contentType: APPLICATION_JSON_VALUE
        )) {
            status == SC_NOT_FOUND
        }

        // check user problems
        with(userClient.get(path: "/api/competition/id/${competitionId}/problem/byUsername/${USER_USERNAME}")) {
            status == SC_OK
            with(responseData) {
                size() == 1

                with(it[0]) {
                    competitionId == competitionId
                    failedAttempts == 1
                    problemTemplateId == 'dummy'
                    !solved
                    username == AbstractControllerSpec.USER_USERNAME
                }
            }
        }

        // generate problem
        def problemId3 = userClient.post(
            path: '/api/problem/',
            query: [
                apiKey: apiKey
            ],
            body: [
                competitionId: competitionId,
                problemTemplateId: 'dummy'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).responseData.id

        // answer correctly
        with(userClient.delete(
            path: "/api/problem/problemId/$problemId3",
            body: [DummyProblemTemplate.EXPECTED_ANSWER],
            contentType: APPLICATION_JSON_VALUE
        )) {
            status == SC_NO_CONTENT
        }

        // check user problems
        with(userClient.get(path: "/api/competition/id/${competitionId}/problem/byUsername/${USER_USERNAME}")) {
            status == SC_OK
            with(responseData) {
                size() == 1

                with(it[0]) {
                    competitionId == competitionId
                    failedAttempts == 1
                    problemTemplateId == 'dummy'
                    solved
                    username == AbstractControllerSpec.USER_USERNAME
                }
            }
        }
    }
}
