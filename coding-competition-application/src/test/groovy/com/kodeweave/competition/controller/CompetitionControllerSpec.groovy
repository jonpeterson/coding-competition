package com.kodeweave.competition.controller

import com.kodeweave.competition.Application
import com.kodeweave.competition.repository.UserProblemRepository

import static org.apache.http.HttpStatus.*
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE

class CompetitionControllerSpec extends AbstractControllerSpec {

    def 'tests'() {
        expect:

        // get user problem repository for later use
        def userProblemRepository = Application.context.getBean(UserProblemRepository)


        // create competition using admin's credentials
        def responseA = adminClient.post(
            path: '/api/competition/',
            body: [
                name: 'Competition A',
                startDate: '2015-01-02T01:23:45-05:00',
                endDate: '2015-01-03T02:33:45-05:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        )
        with(responseA) {
            status == SC_OK
            with(responseData) {
                id != null
                name == 'Competition A'
                startDate == '2015-01-02T06:23:45Z'
                endDate == '2015-01-03T07:33:45Z'
                problemConfigurations == []
                participatingUsers == []
            }
        }
        def competitionA = responseA.responseData

        // create competition using admin's credentials
        def responseB = adminClient.post(
            path: '/api/competition/',
            body: [
                name: 'Competition B',
                startDate: '2015-01-02T01:23:45.000-04:00',
                endDate: '2015-01-03T02:33:45.000-04:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        )
        with(responseB) {
            status == SC_OK
            with(responseData) {
                id != null
                name == 'Competition B'
                startDate == '2015-01-02T05:23:45Z'
                endDate == '2015-01-03T06:33:45Z'
                problemConfigurations == []
                participatingUsers == []
            }
        }
        def competitionB = responseB.responseData

        // create competition using other user's credentials
        unauthenticatedClient.post(
            path: '/api/competition/',
            body: [
                name: 'Competition A',
                startDate: '2015-01-02T01:23:45-05:00',
                endDate: '2015-01-03T02:33:45-05:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_UNAUTHORIZED
        userClient.post(
            path: '/api/competition/',
            body: [
                name: 'Competition C',
                startDate: '2015-01-02T01:23:45-05:00',
                endDate: '2015-01-03T02:33:45-05:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_FORBIDDEN

        // create invalid competition using admin's credentials
        adminClient.post(
            path: '/api/competition/',
            body: [
                name: 'Competition D',
                startDate: '2015-01-02T01:23:45-05:00',
                endDate: 'blah'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_BAD_REQUEST

        // get all competitions
        with(unauthenticatedClient.get(path: '/api/competition/')) {
            status == SC_OK
            responseData.collect { it.name } as Set == [ 'Competition A', 'Competition B' ] as Set
        }

        // get competition
        with(unauthenticatedClient.get(path: "/api/competition/id/${competitionB.id}")) {
            status == SC_OK
            responseData.name == 'Competition B'
        }

        // get non-existent competition
        unauthenticatedClient.get(path: '/api/competition/id/00000000-0000-0000-0000-000000000000').status == SC_NOT_FOUND

        // set name and dates using admin's credentials
        adminClient.put(
            path: "/api/competition/id/${competitionA.id}/name",
            body: 'Competition A+',
            contentType: TEXT_PLAIN_VALUE
        ).status == SC_OK
        adminClient.put(
            path: "/api/competition/id/${competitionA.id}/dates",
            body: [
                start: '2015-01-03T01:23:45-05:00',
                end: '2015-01-04T02:33:45-05:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_OK

        // get competition
        with(unauthenticatedClient.get(path: "/api/competition/id/${competitionA.id}")) {
            status == SC_OK
            with(responseData) {
                name == 'Competition A+'
                startDate == '2015-01-03T06:23:45Z'
                endDate == '2015-01-04T07:33:45Z'
            }
        }

        // set name and dates using non-admin's credentials
        userClient.put(
            path: "/api/competition/id/${competitionA.id}/name",
            body: 'Competition A++',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_FORBIDDEN
        unauthenticatedClient.put(
            path: "/api/competition/id/${competitionA.id}/name",
            body: 'Competition A++',
            contentType: TEXT_PLAIN_VALUE,
            headers: ['Accept': APPLICATION_JSON_VALUE]
        ).status == SC_UNAUTHORIZED
        userClient.put(
            path: "/api/competition/id/${competitionA.id}/dates",
            body: [
                start: '2015-01-04T01:23:45-05:00',
                end: '2015-01-05T02:33:45-05:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_FORBIDDEN
        unauthenticatedClient.put(
            path: "/api/competition/id/${competitionA.id}/dates",
            body: [
                start: '2015-01-04T01:23:45-05:00',
                end: '2015-01-05T02:33:45-05:00'
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_UNAUTHORIZED

        // get competition
        with(unauthenticatedClient.get(path: "/api/competition/id/${competitionA.id}")) {
            status == SC_OK
            with(responseData) {
                name == 'Competition A+'
                startDate == '2015-01-03T06:23:45Z'
                endDate == '2015-01-04T07:33:45Z'
            }
        }

        // now that we have created competitions, add user problem attempts
        userProblemRepository.attempt(UUID.fromString(competitionA.id), USER_USERNAME, 'dummy1', true)
        userProblemRepository.attempt(UUID.fromString(competitionA.id), USER_USERNAME, 'dummy2', false)
        userProblemRepository.attempt(UUID.fromString(competitionA.id), USER_USERNAME, 'dummy3', true)
        userProblemRepository.attempt(UUID.fromString(competitionB.id), USER_USERNAME, 'dummy1', false)
        userProblemRepository.attempt(UUID.fromString(competitionB.id), USER_USERNAME, 'dummy2', true)
        userProblemRepository.attempt(UUID.fromString(competitionA.id), ADMIN_USERNAME, 'dummy3', true)
        userProblemRepository.attempt(UUID.fromString(competitionB.id), ADMIN_USERNAME, 'dummy1', true)

        // configure the problems using admin's credentials
        [
            (competitionA.id): [dummy1: [points: 2, enabled: true], dummy2: [points: 3, enabled: true], dummy3: [points: 5, enabled: false]],
            (competitionB.id): [dummy1: [points: 7, enabled: true]]
        ].each { competitionId, templates ->
            templates.each { templateId, data ->
                adminClient.put(
                    path: "/api/competition/id/${competitionId}/problemConfiguration/problemTemplateId/${templateId}",
                    body: data,
                    contentType: APPLICATION_JSON_VALUE
                )
            }
        }

        // add user using admin's credentials
        adminClient.put(path: "/api/competition/id/${competitionA.id}/participatingUser/username/${USER_USERNAME}").status == SC_OK
        adminClient.put(path: "/api/competition/id/${competitionA.id}/participatingUser/username/${USER2_USERNAME}").status == SC_OK
        adminClient.put(path: "/api/competition/id/${competitionA.id}/participatingUser/username/${ADMIN_USERNAME}").status == SC_OK
        adminClient.put(path: "/api/competition/id/${competitionB.id}/participatingUser/username/${USER_USERNAME}").status == SC_OK
        adminClient.put(path: "/api/competition/id/${competitionB.id}/participatingUser/username/${USER2_USERNAME}").status == SC_OK
        adminClient.put(path: "/api/competition/id/${competitionB.id}/participatingUser/username/${ADMIN_USERNAME}").status == SC_OK

        // re-add a same user using admin's credentials
        adminClient.put(path: "/api/competition/id/${competitionA.id}/participatingUser/username/${USER_USERNAME}").status == SC_OK

        // get all competitions
        with(unauthenticatedClient.get(path: '/api/competition/')) {
            status == SC_OK

            with(responseData.find { it.id == competitionA.id }) {
                with(problemConfigurations.collectEntries { [(it.problemTemplateId): it] }) {
                    size() == 3
                    with(it.dummy1) {
                        points == 2
                        enabled
                    }
                    with(it.dummy2) {
                        points == 3
                        enabled
                    }
                    with(it.dummy3) {
                        points == 5
                        !enabled
                    }
                }

                with(participatingUsers.collectEntries { [(it.username): it] }) {
                    size() == 3
                    with(it[AbstractControllerSpec.USER_USERNAME]) {
                        name == AbstractControllerSpec.USER_NAME
                        points == 2
                    }
                    with(it[AbstractControllerSpec.USER2_USERNAME]) {
                        name == AbstractControllerSpec.USER2_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.ADMIN_USERNAME]) {
                        name == AbstractControllerSpec.ADMIN_NAME
                        points == 0
                    }
                }
            }

            with(responseData.find { it.id == competitionB.id }) {
                with(problemConfigurations.collectEntries { [(it.problemTemplateId): it] }) {
                    size() == 1
                    with(dummy1) {
                        points == 7
                        enabled
                    }
                }

                with(participatingUsers.collectEntries { [(it.username): it] }) {
                    size() == 3
                    with(it[AbstractControllerSpec.USER_USERNAME]) {
                        name == AbstractControllerSpec.USER_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.USER2_USERNAME]) {
                        name == AbstractControllerSpec.USER2_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.ADMIN_USERNAME]) {
                        name == AbstractControllerSpec.ADMIN_NAME
                        points == 7
                    }
                }
            }
        }

        // update problem configuration using admin's credentials
        adminClient.put(
            path: "/api/competition/id/${competitionA.id}/problemConfiguration/problemTemplateId/dummy1/",
            body: [
                points: 11,
                enabled: true
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_OK

        // set problem configuration using admin's credentials
        adminClient.put(
            path: "/api/competition/id/${competitionA.id}/problemConfiguration/problemTemplateId/dummy3/",
            body: [
                points: 5,
                enabled: true
            ],
            contentType: APPLICATION_JSON_VALUE
        ).status == SC_OK

        // get all competitions
        with(unauthenticatedClient.get(path: '/api/competition/')) {
            status == SC_OK

            with(responseData.find { it.id == competitionA.id }) {
                with(problemConfigurations.collectEntries { [(it.problemTemplateId): it] }) {
                    size() == 3
                    with(it.dummy1) {
                        points == 11
                        enabled
                    }
                    with(it.dummy2) {
                        points == 3
                        enabled
                    }
                    with(it.dummy3) {
                        points == 5
                        enabled
                    }
                }

                with(participatingUsers.collectEntries { [(it.username): it] }) {
                    size() == 3
                    with(it[AbstractControllerSpec.USER_USERNAME]) {
                        name == AbstractControllerSpec.USER_NAME
                        points == 16
                    }
                    with(it[AbstractControllerSpec.USER2_USERNAME]) {
                        name == AbstractControllerSpec.USER2_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.ADMIN_USERNAME]) {
                        name == AbstractControllerSpec.ADMIN_NAME
                        points == 5
                    }
                }
            }

            with(responseData.find { it.id == competitionB.id }) {
                with(problemConfigurations.collectEntries { [(it.problemTemplateId): it] }) {
                    size() == 1
                    with(dummy1) {
                        points == 7
                        enabled
                    }
                }

                with(participatingUsers.collectEntries { [(it.username): it] }) {
                    size() == 3
                    with(it[AbstractControllerSpec.USER_USERNAME]) {
                        name == AbstractControllerSpec.USER_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.USER2_USERNAME]) {
                        name == AbstractControllerSpec.USER2_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.ADMIN_USERNAME]) {
                        name == AbstractControllerSpec.ADMIN_NAME
                        points == 7
                    }
                }
            }
        }

        // remove the first using the admin's credentials
        adminClient.delete(path: "/api/competition/id/${competitionA.id}/problemConfiguration/problemTemplateId/dummy1").status == SC_OK

        // remove the first again using the admin's credentials
        adminClient.delete(path: "/api/competition/id/${competitionA.id}/problemConfiguration/problemTemplateId/dummy1").status == SC_OK

        // remove user using admin's credentials
        adminClient.delete(path: "/api/competition/id/${competitionA.id}/participatingUser/username/${USER_USERNAME}").status == SC_OK

        // remove same user using admin's credentials
        adminClient.delete(path: "/api/competition/id/${competitionA.id}/participatingUser/username/${USER_USERNAME}").status == SC_NOT_FOUND

        // get all competitions
        with(unauthenticatedClient.get(path: '/api/competition/')) {
            status == SC_OK

            with(responseData.find { it.id == competitionA.id }) {
                with(problemConfigurations.collectEntries { [(it.problemTemplateId): it] }) {
                    size() == 2
                    with(it.dummy2) {
                        points == 3
                        enabled
                    }
                    with(it.dummy3) {
                        points == 5
                        enabled
                    }
                }

                with(participatingUsers.collectEntries { [(it.username): it] }) {
                    size() == 2
                    with(it[AbstractControllerSpec.USER2_USERNAME]) {
                        name == AbstractControllerSpec.USER2_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.ADMIN_USERNAME]) {
                        name == AbstractControllerSpec.ADMIN_NAME
                        points == 5
                    }
                }
            }

            with(responseData.find { it.id == competitionB.id }) {
                with(problemConfigurations.collectEntries { [(it.problemTemplateId): it] }) {
                    size() == 1
                    with(dummy1) {
                        points == 7
                        enabled
                    }
                }

                with(participatingUsers.collectEntries { [(it.username): it] }) {
                    size() == 3
                    with(it[AbstractControllerSpec.USER_USERNAME]) {
                        name == AbstractControllerSpec.USER_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.USER2_USERNAME]) {
                        name == AbstractControllerSpec.USER2_NAME
                        points == 0
                    }
                    with(it[AbstractControllerSpec.ADMIN_USERNAME]) {
                        name == AbstractControllerSpec.ADMIN_NAME
                        points == 7
                    }
                }
            }
        }

        // get problems by template id
        with(unauthenticatedClient.get(path: "/api/competition/id/${competitionA.id}/problem/byTemplateId/dummy3")) {
            status == SC_OK

            responseData.collect { "${it.competitionId}|${it.username}|${it.problemTemplateId}|${it.solved}|${it.failedAttempts}" } as Set == [
                "${competitionA.id}|${AbstractControllerSpec.USER_USERNAME}|dummy3|true|0",
                "${competitionA.id}|${AbstractControllerSpec.ADMIN_USERNAME}|dummy3|true|0"
            ] as Set
        }

        // get problems by username
        with(unauthenticatedClient.get(path: "/api/competition/id/${competitionA.id}/problem/byUsername/${AbstractControllerSpec.USER_USERNAME}")) {
            status == SC_OK

            responseData.collect { "${it.competitionId}|${it.username}|${it.problemTemplateId}|${it.solved}|${it.failedAttempts}" } as Set == [
                "${competitionA.id}|${AbstractControllerSpec.USER_USERNAME}|dummy1|true|0",
                "${competitionA.id}|${AbstractControllerSpec.USER_USERNAME}|dummy2|false|1",
                "${competitionA.id}|${AbstractControllerSpec.USER_USERNAME}|dummy3|true|0"
            ] as Set
        }

        // get all competitions for username
        with(unauthenticatedClient.get(path: "/api/competition/byUsername/${AbstractControllerSpec.USER_USERNAME}")) {
            status == SC_OK

            responseData.collect { it.id } == [ competitionB.id ]
        }
    }
}
