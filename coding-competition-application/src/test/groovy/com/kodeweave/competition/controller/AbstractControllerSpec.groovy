package com.kodeweave.competition.controller

import com.kodeweave.competition.Application
import com.kodeweave.competition.controller.dto.CreateUserInDto
import com.kodeweave.competition.model.User
import com.kodeweave.competition.repository.mongo.model.MongoUser
import groovyx.net.http.RESTClient
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Query
import org.springframework.security.authentication.TestingAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Shared
import spock.lang.Specification
import test.CustomRESTClient

abstract class AbstractControllerSpec extends Specification {
    static final BASE_URL = 'http://localhost:8080/api/'

    static final USER_USERNAME = 'adam'
    static final USER_EMAIL = 'user@test.com'
    static final USER_PASSWORD = 'password'
    static final USER_NAME = 'Adam'
    static final USER2_USERNAME = 'bob'
    static final USER2_EMAIL = 'user2@test.com'
    static final USER2_PASSWORD = 'password'
    static final USER2_NAME = 'Bob'
    static final ADMIN_USERNAME = 'charlie'
    static final ADMIN_EMAIL = 'admin@test.com'
    static final ADMIN_PASSWORD = 'password'
    static final ADMIN_NAME = 'Charlie'

    protected static RESTClient createClient(String username = null, String password = null) {
        def client = new CustomRESTClient(BASE_URL)
        client.handler.failure = client.handler.success

        if(username != null)
            client.headers['Authorization'] = 'Basic ' + "$username:$password".bytes.encodeBase64()

        client
    }


    @Shared RESTClient unauthenticatedClient
    @Shared RESTClient userClient
    @Shared RESTClient user2Client
    @Shared RESTClient adminClient

    @Shared User user
    @Shared User user2
    @Shared User admin

    def setupSpec() {
        if(!Application.started)
            Application.start()

        unauthenticatedClient = createClient()
        userClient = createClient(USER_USERNAME, USER_PASSWORD)
        user2Client = createClient(USER2_USERNAME, USER2_PASSWORD)
        adminClient = createClient(ADMIN_USERNAME, ADMIN_PASSWORD)
    }

    def setup() {
        def authentication = new TestingAuthenticationToken(new User([ roles: [User.Role.ADMIN] ]), 'test', [User.Role.ADMIN.authority])
        authentication.authenticated = true
        SecurityContextHolder.context.authentication = authentication

        Application.context.with {
            getBean(MongoTemplate).remove(new Query(), MongoUser)

            def userController = getBean(UserController)
            userController.create(USER_USERNAME,  new CreateUserInDto(email: USER_EMAIL,  password: USER_PASSWORD,  name: USER_NAME), null)
            userController.create(USER2_USERNAME, new CreateUserInDto(email: USER2_EMAIL, password: USER2_PASSWORD, name: USER2_NAME), null)
            userController.create(ADMIN_USERNAME, new CreateUserInDto(email: ADMIN_EMAIL, password: ADMIN_PASSWORD, name: ADMIN_NAME), null)
            userController.addRole(ADMIN_USERNAME, User.Role.ADMIN)
        }
    }
}
