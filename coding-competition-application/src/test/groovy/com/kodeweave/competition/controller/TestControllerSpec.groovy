package com.kodeweave.competition.controller

import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpSession

import static org.apache.http.HttpStatus.*

class TestControllerSpec extends AbstractControllerSpec {

    @RestController
    @RequestMapping('/api/test/security')
    static class TestController {

        @RequestMapping('/anyone')
        void anyone() {
        }

        @RequestMapping('/user')
        @PreAuthorize('isAuthenticated()')
        void user() {
        }

        @RequestMapping('/admin')
        @PreAuthorize('hasRole("ADMIN")')
        void admin() {
        }

        @RequestMapping(value = '/role', produces = 'application/json')
        @PreAuthorize('isAuthenticated()')
        List<String> roles() {
            SecurityContextHolder.context.authentication.authorities.collect { it.authority }
        }

        @RequestMapping('/logout')
        @ResponseStatus(HttpStatus.NO_CONTENT)
        void logout(HttpSession session) {
            session.invalidate()
        }
    }


    def 'ensure sessions and general security is configured'() {
        expect:

        // get non-existant endpoint
        unauthenticatedClient.get([path: '/api/test/security']).status == SC_NOT_FOUND

        // successfully get public endpoint with no credentials
        unauthenticatedClient.get([path: '/api/test/security/anyone']).status == SC_OK

        // get endpoint that requires authentication with no credentials
        unauthenticatedClient.get([path: '/api/test/security/user']).status == SC_UNAUTHORIZED

        // successfully get endpoint that requires authentication with credentials
        def tokenClient = createClient()
        with(userClient.get([path: '/api/test/security/user'])) {
            status == SC_OK
            (tokenClient.headers['x-auth-token'] = allHeaders.collectEntries({ it.buffer.toString().split(': ', 2) as List })['x-auth-token']) != null
        }

        // successfully get endpoint that requires authentication with session token
        tokenClient.get([path: '/api/test/security/user']).status == SC_OK

        // get endpoint that requires ADMIN role
        tokenClient.get([path: '/api/test/security/admin']).status == SC_FORBIDDEN

        // successfully invalid the session
        tokenClient.get([path: '/api/test/security/logout']).status == SC_NO_CONTENT

        // get endpoint that requires authentication with invalidated session token
        tokenClient.get([path: '/api/test/security/user']).status == SC_UNAUTHORIZED

        // successfully get endpoint that requires ADMIN role
        adminClient.get([path: '/api/test/security/admin']).status == SC_OK
    }
}
