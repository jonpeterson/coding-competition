package com.kodeweave.competition.problem

import org.springframework.stereotype.Component

@Component
class DummyProblemTemplate extends ProblemTemplate<String, String> {
    static final String ID = 'dummy'
    static final String PARAMETER = 'some parameter'
    static final String EXPECTED_ANSWER = 'expectedAnswer'

    DummyProblemTemplate() {
        super(ID, 'dummy problem template', 1000)
    }

    @Override
    protected List<ParametersAndAnswer<String, String>> generateParametersAndAnswerList(Random random) {
        return [ new ParametersAndAnswer<String, String>(PARAMETER, EXPECTED_ANSWER) ]
    }
}
