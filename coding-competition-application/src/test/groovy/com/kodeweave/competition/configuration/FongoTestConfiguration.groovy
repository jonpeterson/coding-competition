package com.kodeweave.competition.configuration

import com.github.fakemongo.Fongo
import com.mongodb.MongoClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FongoTestConfiguration {

    @Bean
    MongoClient fongo() {
        return new Fongo('Test Fongo').getMongo()
    }
}
