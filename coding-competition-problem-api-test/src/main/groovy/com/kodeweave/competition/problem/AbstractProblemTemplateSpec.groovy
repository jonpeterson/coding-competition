package com.kodeweave.competition.problem

import spock.lang.Specification

abstract class AbstractProblemTemplateSpec<PARAMETERS, ANSWER> extends Specification {

    protected abstract ProblemTemplate<PARAMETERS, ANSWER> createTemplate()

    protected abstract ANSWER solve(PARAMETERS parameters)

    def 'solve 100 times'() {
        expect:
        def template = createTemplate()

        100.times {
            def instance = template.generateInstance(null, null, new Random())
            def answers = instance.parametersList.collect { solve(it) }
            instance.answer(answers)
            if(!instance.solved)
                throw new IllegalStateException("\nexpected: ${instance.answersList}\nactual: ${answers}")
        }
    }
}
