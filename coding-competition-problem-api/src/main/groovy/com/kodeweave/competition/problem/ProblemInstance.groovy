package com.kodeweave.competition.problem

import groovy.transform.EqualsAndHashCode
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@EqualsAndHashCode(includes = ['id'], cache = true)
class ProblemInstance<PARAMETERS, ANSWER> {
    private static final Logger logger = LoggerFactory.getLogger(ProblemInstance)


    UUID id
    UUID competitionId
    String username
    ProblemTemplate<PARAMETERS, ANSWER> template
    List<PARAMETERS> parametersList
    List<ANSWER> answersList
    Long expireTime
    boolean solved

    void answer(List<ANSWER> attemptedAnswersList) {
        solved = attemptedAnswersList == answersList

        logger.debug("$username attempted to answer problem of type ${template.id} in competition '$competitionId':\n  attempted: $attemptedAnswersList\n  expected: $answersList\n  result: $solved")
    }

    boolean isExpired() {
        return expireTime != null && System.currentTimeMillis() >= expireTime
    }
}
