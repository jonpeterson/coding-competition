package com.kodeweave.competition.problem

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes = ['id'])
abstract class ProblemTemplate<PARAMETERS, ANSWER> {

    protected static int randomInt(Random random, int low, int high) {
        return low + random.nextInt(high - low + 1)
    }

    protected static String randomText(Random random, int length) {
        def text = ''
        length.times {
            text += randomInt(random, 65, 90) as char
        }
        return text
    }


    final String id
    final String description
    final int timeToLiveMilliseconds

    ProblemTemplate(String id, String description, int timeToLiveMilliseconds) {
        this.id = id
        this.description = description
        this.timeToLiveMilliseconds = timeToLiveMilliseconds
    }

    ProblemInstance<PARAMETERS, ANSWER> generateInstance(UUID competitionId, String username, Random random) {
        List<ParametersAndAnswer<PARAMETERS, ANSWER>> parametersAndAnswerList = generateParametersAndAnswerList(random)

        Collections.shuffle(parametersAndAnswerList, random)

        return new ProblemInstance<PARAMETERS, ANSWER>(
            id: UUID.randomUUID(),
            competitionId: competitionId,
            username: username,
            template: this,
            parametersList: parametersAndAnswerList.collect { it.parameters },
            answersList: parametersAndAnswerList.collect { it.answer },
            expireTime: System.currentTimeMillis() + timeToLiveMilliseconds
        )
    }

    protected abstract List<ParametersAndAnswer<PARAMETERS, ANSWER>> generateParametersAndAnswerList(Random random)
}
