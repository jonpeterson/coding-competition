package com.kodeweave.competition.problem

class ParametersAndAnswer<PARAMETERS, ANSWER> {
    final PARAMETERS parameters
    final ANSWER answer

    ParametersAndAnswer(PARAMETERS parameters, ANSWER answer) {
        this.parameters = parameters
        this.answer = answer
    }
}
